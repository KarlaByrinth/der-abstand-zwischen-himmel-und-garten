## Anmerkungen zu den Content Notes

Ich versuche hier eine möglichst vollständige Liste an Content Notes
zur Verfügung zu stellen, aber weiß, dass ich nicht immer alles
auf dem Schirm habe. Hinweise sind willkommen und werden ergänzt. Über
die Content Notes hinaus darf mir gern jede Frage nach Inhalten gestellt
werden und ich spoilere in privaten Konversationen nach bestem
Wissen. Es bedarf dafür keiner Begründung oder Diskussion. Ich mache das
einfach.

Ich nehme außerdem teils sehr seltene Content Notes für Personen mit auf, die
ich kenne, weil sie sich für meine Kunst interessieren.

### Für das ganze Buch

**Zentrale Themen:**

- Blut.
- Geist.
- Insekten erwähnt.

**Randthemen:**

- Schmerzen.
- Wirbellose Tiere.
- Lookism.
- Altersshaming.
- Nacktheit.
- Algen.
- Klimawandel.
- Krieg.
- Mikroaggressionen im Zusammenhang mit Nichtbinärfeindlichkeit und Allonormativität.
- Misgendern.
- Nichtbinärfeindlichkeit.

### 01 - BGE

- Blut.
- Geist.

### 03 - Umzug

- Insekten erwähnt.

### 05 - Plan Los

- Schmerzen.
- Wirbelloses Tier.

### 07 - Winden

- Lookism.
- Altersshaming.
- Nacktheit.
- Algen.
- Klimawandel.
- Mikroaggressionen im Zusammenhang mit Nichtbinärfeindlichkeit und Allonormativität.

### 08 - Wellt

- Wirbellose Tiere.
- Klimawandel.
- Krieg.

### 09 - Zurück

- Klimawandel.
- Misgendern.
- Nichtbinärfeindlichkeit.

###  Entwicklung, Danksagung, Referenzne, Eastereggs und Metagedöns

- Wirbellose Tiere.
