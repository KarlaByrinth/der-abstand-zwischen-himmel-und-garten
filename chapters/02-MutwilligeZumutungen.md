Mutwillige Zumutungen
=====================

\Beitext{Ranuk}

Ranuk hatte sich durchaus Gedanken darüber gemacht, ob es eine gute
Idee wäre, eine Person um Hilfe zu bitten, mit ihsem ganzen Kram fertig
zu werden. Ihs Umfeld bestand im Wesentlichen aus Online-Kontakten, die
rie auch gern genug hatte, sie im Outernet treffen zu mögen, wenn
rie denn mobil gewesen wäre. Die Kontakte waren fast alle zwischen 20 und
50 Jahre jünger als Ranuk. Das ergab sich so, wenn eine ältere
Person herausfand, dass sie nicht-binär war, und dann in diesem
Internet nach Kontakten für Austausch darüber suchte. Rie war diesen
Frühling 67 geworden. Rie mochte all die Kontakte, hatte darunter
innige freundschaftliche Beziehungen, aber rie konnte auch nicht
sagen, dass es ihs nichts ausmachte, so viel älter zu sein.

Eins hätte meinen können, dass unter den Kontakten viele ihs hätten helfen
können, aber trotz des Age Gaps waren die meisten ihser Kontakte nicht unbedingt
besser in der Lage, schwere Arbeit zu verrichten, geschweige denn
hatten sie Energie. Es war nicht nur
eine queere, sondern auch eine behinderte Community, in die rie
hineingewachsen war. Und das war auch schön. Trotzdem hätte rie
sich das bei so zwei bis drei Personen überlegen können, ob rie
um Hilfe bitten sollte, und hätte sie wahrscheinlich auch bekommen. Aber
rie hatte sich dagegen entschieden.

Rie war zu dem Schluss gekommen, dass rie sich eben nie
überwinden könnte, eine noch so vertraute Person in ihse Räume zu lassen. Nicht
in dem Zustand. Rie hätte vorher wenigstens etwas der Arbeit selbst erledigen
wollen, etwa sich des Papiermülls entledigen, der sich stapelte, gegen den rie nicht
ankam, oder die seit Jahren zugestaubten Ecken wischen wollen, die vermutlich hochgradig
ungesund waren, aber an die rie einfach nicht dran kam. Der Zustand wurde allerdings
eher schlimmer als besser.

Interessanterweise fand rie die Idee, eine fremde Person zu fragen,
einfacher, als eine Person aus ihsem Umfeld zu fragen. Einfacher.
Komparativ. Immer noch meilenweit entfernt von einfach.

Ranuk setzte sich erschöpft an den Schreibtisch, nachdem rie all die
Spiegel gewischt hatte. Das einzige, was in diesem Haushalt sehr regelmäßig
sehr sauber war. Vom Schreibtisch aus konnte rie hinaus in den Garten
schauen. Wenn sich das Garten nennen konnte. Ein einziger gemähter Streifen
führte durch hohes Gras und ausgewucherte Hecke hindurch zum Schuppen. Mehr
schaffte rie nicht.

Es war eigentlich zu schönes Wetter, um vorm Computer zu hängen und
Escube zu spielen. Aber was sonst tun. Eine von den vielen
Aufgaben wohl. Und sei es, den Weg zum Schuppen noch einmal zu mähen. Aber
dafür müsste rie das Kabel aus dem Schuppen schleppen und durch Teile
des Hauses verlegen. Und rie war so erschöpft, dass wenn dabei was nicht
so liefe, wie gewünscht, es zu einem mentalen Breakdown
führen würde.

Rie schleppte sich doch noch einmal nach unten, einfach um sich die Sache
anzusehen. Manchmal half das schon, damit für den Zeitpunkt, zu dem rie
dann das Mähen in Angriff nahm, ein bisschen mehr Energie da war, weil der
Kopf schon passend orientiert war.

Auf dem Rückweg kam Ranuk am Flurspiegel vorbei. 'Tu es!', stand
darauf, in weniger dramatischen Lettern. Oben links, fast zurückhaltend
und vielleicht ein bisschen flehend.

"Aber ich müsste dann eine fremde Person in ein Haus lassen, in
dem du mit Blut Nachrichten an jeden freien Spiegel schreibst", erwiderte
Ranuk freundlich. "Damit wirst du dann ja nicht aufhören, nehme ich an?" Inzwischen
konnte sich das nur noch auf Emre beziehen, oder?

Rie setzte sich an den Computer und öffnete den Link noch einmal. Rie war
nur mäßig skeptisch, dem Geist zu trauen. Auch wenn sich noch nie
ein Rat als negativ herausgestellt hatte, ließ rie sich natürlich
nicht herumkommandieren, aber auf einen "Vorschlag" des Geists hin, sich
die Sache noch einmal ansehen, konnte nicht schaden.

Emres Text hatte sich nicht geändert. Es gab einen Button, auf dem
'Kontaktieren' stand, den Ranuk drückte. Einfach erstmal ausprobieren, was
sich dann öffnete, sich die Sache genauer ansehen. Die AGB war
als eine der Standard-AGBs markiert, die Ranuk von aktivistischen Portalen
schon kannte. Das war immerhin gut. Eine Seite, die sich klar an
progressiv anti-kapitalistische Standards hielt.

Das Kontaktformular sah auch sehr langweilig aus. Rie trug ihse Mail-Adresse
ein und begutachtete dann das große graue Feld, in das rie ihse Nachricht
tippen könnte. Aber was tippte eins da? Die Wahrheit? Dass alles, was
Ranuk zu "bieten" hätte, eine Zumutung wäre?

Rie tippte 'Zuumuutung' ins Nachrichtenfeld, einfach so, mit einer leicht
übertriebenen Anzahl 'u's. U war ein guter Buchstabe. Rie wollte zwei
Zeilenumbrüche hinzufügen, einfach um noch ein wenig in dem Eingabefeld zu
brainstormen, drückte dafür wie für Chat-Programme gewohnt Shift + Enter, und
schickte auf diese Weise ungewollt die Nachricht ab. Es war ja eben kein
Chat-Programm.

Ihs Körper brauchte einen müden Moment um zu reagieren, bis er heiß
anlief und sich durch und durch schämte.

Nun musste Ranuk wohl oder übel eine Nachricht hinterherschicken und
den Mist aufklären. Einfach schreiben, dass es ein Versehen gewesen
wäre? Die Schuld gar auf den Computer schieben? Irgendwas
hinterherschicken, das nahelegte, dass rie ein Bot wäre? Aber
das wäre gelogen. Vielleicht spielte das keine Rolle, aber irgendwie
wollte Ranuk ehrlich sein. Weil die Anzeige auch sehr ehrlich geklungen
hatte.

Noch während Ranuk sich Gedanken machte, kam bereits eine
Rückmail von Emre. Die Popup-Nachricht zeigte den Text mit an, auf
den Emre reagierte: 'Zuumuutung'.

> Meinst du, dass ich oder meine Anzeige eine Zuumuutung wäre, oder
> dass du mir etwas zuumuuten willst. Außer dieser Nachricht, meine
> ich?
> 
> Emre

Die Scham ließ kaum nach, aber ein Schmunzeln schlich sich dazwischen. Ranuk
überlegte, dass eine rasche Nachricht besser wäre, als eine gut durchdachte. Zumal
eine gut durchdachte vielleicht auch weder ihse Stärke wäre, noch etwas, was rie
vor morgen hinbekommen hätte.

> Entschuldige.

Rie übernahm das duzen. Und fügte dem Satz dann doch noch ein 'bitte' hinzu.

> Meine Mail, mein "Angebot" und ich wären alle zusammen genommen eine
> Zumutung. Ich hatte eigentlich nicht geplant, das abzuschicken.
> 
> Ranuk

Rie schickte die Nachricht kurz und schmerzlos ab. Ihs Kopf fragte
rie in leuchtenden Buchstaben: Was zum Slik machst du da?

Die Scham beruhigte sich weiterhin kaum. Atmen. Langsam ein- und ausatmen. Vielleicht
gleich doch eine Runde Escube zocken. Das beruhigte. Es überraschte Ranuk
allerdings nicht, von dem Ansinnen von einer weiteren Mail abgelenkt zu
werden.

> Erzähl trotzdem, wenn du magst. Und wenn du das ernst meinst. Ich
> kann ja selbst entscheiden, ob ich ablehne. Tu gern so, als wäre
> nichts zu unverschämt.
> 
> Emre

Ranuk seufzte. Nun also die Lage kurzfassen. Rie konnte nicht
leugnen, dass Emre nette und einladende Nachrichten schrieb.

> Ich weiß nicht, ob ich mir schon das Label messy geben darf. Jedenfalls
> habe ich ein kleines Haus mit Grundstück und jeder Menge Chaos. Mein
> Traum ist, daraus etwas zu machen, was sich mehr nach mir anfühlt. Wahlweise
> auch einfach nach was anderem als nach meinen verstorbenen Eltern, die hier mal
> gewohnt haben.

So etwas wäre doch sogar hoffentlich auch dann verständlich, wenn dabei der
Hintergrund nicht bekannt war, dass sie kein gutes Verhältnis zueinander
gehabt hatten. Den Geist -- im mehr metaphorischen Sinne -- der Eltern
aus dem Grundstück treiben zu wollen, weil Emotionen, wahlweise einfach Trauer,
daran hingen, war doch recht normal, oder? Der hier hausende Geist
würde sich dann sicher auch wohler fühlen. Hoffte Ranuk.

Rie machte einen Absatz -- dieses Mal, ohne aus Versehen schon abzuschicken --, und führte fort:

> Die Unverschämtheit besteht nicht nur in der Dimension des Chaos', sondern
> auch darin, dass ich niemanden in mein Haus lasse. Das ist mein
> Privatreich. Ich habe einen Schuppen im Garten, der eine Spüle
> mit Wasseranschluss hat. Aber auch zugewuchert, undicht und
> vollgerümpelt ist.
> 
> Meine merkwürdige Nachbarin meinte, ich könne das trotzdem als Camping-Dings
> vorschlagen. Ich bin nicht davon überzeugt.
> 
> Daher: Zuumuutung. Was ich so meinte, aber eigentlich nicht hatte
> abschicken wollen.
> 
> Ranuk

Emre antwortete doch bestimmt schon bald wieder. Vielleicht auch so
eine Computer-Ratte. Reichte das W-LAN bis zur Hütte? Wahrscheinlich.
Sonst ließe sich sicher was mit Freifunkroutern basteln. Ranuk grinste, weil
die Überlegungen zu unsinnig erschienen.

> Wie sieht das mit einem Klo aus?
> 
> Emre

Ähm, ja. Da war kein Klo. Und im Haus gab es auch nur eines. Und da
waren Spiegel drin. Das ging nicht. Es sei denn...

> Zur Erinnerung: Du meintest, ich solle so tun, als wäre nichts
> zu unverschämt.
> 
> Es gibt keins in der Hütte, aber es gibt eines im Haus. Würdest du
> dort nur mit verbundenen Augen aufs Klo gehen?
> 
> Ranuk

Ranuk musste grinsen, weil es so eine harte Unverschämtheit war.

> Bei deiner Einleitung dachte ich schon, du wolltest mir einen
> Pisspott oder eine Bettpfanne vorschlagen. Und auch das hätte
> ich erwogen.
>
> Mit Augenbinde sollte machbar sein.
> 
> Unsere Nachrichten sind so kurz, da fände ich irgendwie ein Chat-Programm
> übersichtlicher. Hast du *Diner*, *shortspread* oder *record*? Würde
> das für dich passen?
> 
> Emre

Wow.

Ranuk saß einige Momente fast fassungslos da. In ihs bildeten sich
Sätze, um zu reagieren, die besagte Fassungslosigkeit in Worte
rahmten, aber rie hatte gleichzeitig das Bedürfnis, Emre ohne unnötige
Ausflüge in ihse Emotionswelt den Wunsch zu erfüllen, zu einem der Chat-Programme zu
wechseln. Um Emre wenigstens in einem Punkt entgegen zu kommen, während
Emre quasi alles bot. Aber was, wenn sie gerade so in ein Chat-Programm
umgezogen waren und sich dann herausstellte, dass das nichts wurde? Was
ja immerhin wahrscheinlich war.

Nun, es gab ja auch die Funktion, sich wieder zu entkontakten. Im
Zweifel zu blocken. Das wäre nicht das Ende der Welt.

> record, Ranuk#9872638
>
> Ranuk

*record* hatte die Möglichkeit, mehrere Nicknames mit einer Kennnummer zu
speichern, die für Ranuk derselbe Account waren, aber für die andere
Seite jeweils eine von mehreren Identitäten.

Es brauchte kaum eine Minute, bis im entsprechenden Programm eine
Nachricht von Emre erschien, die einfach nur aus *Hi* bestand.

Ranuk atmete noch einmal mit geschlossenen Augen bewusst ein und
aus, ein etwas verstörtes, halbes Grinsen im Gesicht, von dem rie
nicht sicher war, ob es dahingehörte, bevor rie sich in den Chat
stürzte.

> *Ranuk:* Es bleibt eine Zuumuutung. Ich weiß nicht, ob ich das
> über mich bringe. Das war alles so nicht geplant.
> 
> *Emre:* Das Wort Zumutung (ich schreibe es zur Abwechslung mit
> nur drei 'u') könnte auch so etwas wie Mut zusprechen heißen.
> Wie Zutoasten, nur dass es eine Ansprache, um Mut zu machen,
> ist und nicht ein Toast zu Ehren einer Person bei einem Fest.
> 
> *Ranuk:* Du meinst, ich mutiviere dich irgendwie?
> 
> *Emre:* Ich jedenfalls bin mutwillig.

Ranuk grinste unwillkürlich etwas überzeugter. Das war netter
Humor, fand rie. Ehe rie sich auf das Antworten konzentrieren
konnte, sah rie, dass Emre schon wieder tippte. Also wartete rie.

> *Emre:* Ich finde es bis jetzt gut, dass du mich angeschrieben
> hast. Ich würde eigentlich am liebsten bei einem
> Haufen Arbeit helfen, der nicht so verkopft ist und bei dem
> ich schnell Veränderungen sehe. Und am liebsten einer Person,
> die es nicht so leicht hat, an entsprechende Hilfe zu kommen. Ich
> bin außerdem ein bisschen Team-scheu. Und ich bringe einen
> BurnOut mit, was mich nur so semi-attraktiv macht, bei Leuten, die
> zeitkritische Arbeit haben.
> 
> Ich mag Herausforderungen
> und seltsame Einschränkungen durchaus, wenn sie nicht
> zu krass sind. Wenn du also magst, lass uns schauen, ob wir
> kompatibel sind. Wohnst du am Meer?
>
> *Ranuk:* Aktuell habe ich eher das Gefühl, du machst mir Mut,
> statt umgekehrt.
> 
> *Emre:* Wirkt es?

Ranuk seufzte. Rie sollte es vielleicht tatsächlich drauf ankommen
lassen.

> *Ranuk:* Meer, bzw Ozean, liegt etwa eine halbe Stunde zu Fuß von hier, mit
> meinen müden Füßen zumindest. Die
> nächste größere bekannte Stadt ist Høppla in Skandern.
> 
> *Emre:* Hui, das ist wirklich weit nördlich.
> 
> *Ranuk:* Dicht am Polarkreis. Im Sommer wird es hier nachts nur
> recht kurz dunkel. Im Winter dagegen tags nur recht kurz hell.
> 
> *Emre:* Ich bin in Nyanberg geboren und habe da bis zum Studium
> gelebt. Studium war in Minzter für drei Jahre, was ich
> teils mochte, weil es am Meer war, aber auf
> der anderen Seite war es mir im Sommer viel zu heiß. Für den
> Job bin ich dann nach Fork gezogen.
> 
> *Ranuk:* Große Großstädte. Dagegen ist selbst Høppla ein Kaff. Du
> arbeitest dich langesam nach Norden. Wobei
> selbst Fork sich von hier aus nach tiefstem
> Süden anfühlt. Hast du überhaupt wintertaugliche Kleidung oder
> müsste ich dir was leihen?
> 
> *Emre:* Gibt es in Høppla keine Läden?
> 
> *Ranuk:* Schon, aber weil es so weit ab vom Schuss ist, ist das
> Pflaster hier recht teuer. Dafür ist die Qualität recht gut.
> Aber ich würde dir vermutlich raten, wenn du dir was anschaffen
> willst, das vorher in Fork zu tun, abgesehen vielleicht von
> einem ordentlichen Regenmantel.
> 
> *Emre:* Regnet es viel?
> 
> *Ranuk:* Sagen wir, es wettert viel. Es gibt hier Sonnentage, oft
> gut Wind, und wenn Regen, dann richtig, und im Winter viel
> Schnee. Allgemein einfach eine gefühlt größere Auswahl Wetter als im
> Süden, nur durchschnittlich wohl kühler als alles, was du kennst.

Unterhielten sie sich über das Wetter? War das dadurch definitionsgemäß
Smalltalk?

> *Emre:* Darauf freue ich mich. Mir ist ohnehin immer zu warm.

War nun schon abgemacht, dass Emre käme?

> *Ranuk:* Das war das Wetter. Was für Faktoren sind für dich noch
> wichtig.
> 
> *Emre:* Ich müsste rausfinden, wie ich hinkomme. Und ich hätte schon
> gern vorher ein paar genauere Informationen dazu, was mich erwartet.
> Unter anderem vielleicht Fotos von der Hütte und eine grobe Idee, ob
> und wie ich es provisorisch dichten kann. Ich möchte nicht direkt mit feuchten
> Träumen anfangen.
> 
> *Ranuk:* Äh.
> 
> *Emre:* Oh shit, ich wollte ein Wortspiel falsch verwenden, und
> habe vergessen, darauf zu achten, worauf das anspielt. Es tut mir sehr
> leid! Das war echt ungünstig und überhaupt nicht geplant.
> 
> *Ranuk:* Dann sind wir quit mit den Peinlichkeiten?

Dieses Mal brauchte es ein paar Momente, bis Emre wieder tippte. Ranuk
stellte sich vor, wie Emre sehr heiß und unbehaglich würde, und fügte
vorsichtshalber, einfach falls es so wäre, schnell noch zwei Sätze hinzu.

> *Ranuk:* Wegen der Mail mit dem Inhalt 'Zuumuutung', meine ich. Ich
> nehme dir nichts übel, wie sollte ich, nach dem ganzen Mutwillen?
> 
> *Emre:* Ü
> 
> Ich habe den Eindruck, du versuchst mir zuzutrosten.
> 
> *Ranuk:* Wirkt es?
> 
> Was bedeutet Ü?
> 
> *Emre:* Ein Ü eignet sich als übertrieben grinsender Smily.

Das konnte Ranuk nicht leugnen. Rie hob für sich eine Augenbraue
und grinste gleichzeitig.

> *Ranuk:* Ü

Und dann ließ Ranuk zu, die Überlegungen nicht mehr für komplett
abwegig zu halten.

> *Ranuk:* Ich kann dir Bilder schicken. Ich habe außerdem ein altes
> Zelt, das vermutlich schon sehr kaputt ist, aber die Bodenplane könnte
> als provisorische Dichtung taugen. Mir kommt es immer noch wie
> eine Unverschämtheit meinerseits vor. Aber vielleicht gewöhne ich mich
> ja noch dran.
