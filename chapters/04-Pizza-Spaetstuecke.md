Pizza-Spätstücke
================

\Beitext{Ranuk}

Ranuk schaute abermals aus dem Fenster in den Garten hinab. Nichts
rührte sich. Irgendein Restanteil Höflichkeit in ihs drängte rie
dazu, Emre Frühstück anzubieten. Oder vielleicht noch einmal
abzusprechen, wo es losgehen sollte, was Emre zuerst tun sollte und
wie.

Ranuk fürchtete sich außerdem davor, hier oben zu sitzen und
Escube zu spielen, während da unten eine Person für rie
arbeitete. Was hatte rie sich eigentlich dabei gedacht, eine
Person mit ihser Arbeit zu beauftragen, wo rie sich doch
stets so furchtbar fühlte, beim Arbeiten zuzugucken. Vielleicht
sollte rie versuchen, zu helfen. Ja, das war es. Rie konnte sich
schwer zu Arbeit überwinden, aber wenn eine andere Person
schon etwas tat, ging es vielleicht ein wenig.

Ranuk schaute abermals aus dem Fenster in den Garten hinab. Immer
noch rührte sich nichts. So ging das schon den ganzen Vormittag. Die
im Ofen aufgewärmten Tiefkühlbrötchen waren längst kalt und
hart. Sie noch einmal aufwärmen würde sie schon recht überknusprig machen. Das
war dann nicht edel oder so etwas. Ranuk hatte keine Ahnung, was Emre
gewohnt war.

Kurz vor Mittag wurde Ranuks Hunger zu groß. Es bekam rie auch
nicht, zu lange nicht zu frühstücken, und rie wusste auch nicht, ob
Emre nicht vielleicht auch lieber alleine frühstücken wollte. Also schmierte
sich Ranuk die Brötchen und aß sie, wie immer vorm Rechner, sich
dabei mit ein paar Leuten via Chat unterhaltend. Die Gespräche
waren ein bisschen anders als üblich. Sie bestanden zwar auch sonst oft im
Wesentlichen aus Herzchen, aber Ranuk war gerade aufgeregt, und üblicherweise
teilte rie die Gefühle in solchen Momenten in den Gesprächen mit. Gerade war aber eine fremde
Person involviert, über die rie noch nicht reden wollte. Irgendwie
war Ranuk ängstlicher über die Unterstützung zu reden, die rie
jetzt hatte, und die Art, wie es dazu gekommen war, als über
die eigene Queerness zu informieren und zu schwärmen oder
so etwas. Das war interessant.

Ranuk wurde von einem Rumpeln aus den Gedanken gerissen. Es kam von
unten aus dem Badezimmer. Das musste Emre sein, mit verbundenen
Augen. Vielleicht war Emre über die Wäschewanne gestolpert.

Wie merkwürdig wäre es, wenn Ranuk nun nach unten ginge und fragte, ob
Emre frühstücken wollte? Oder Hilfe bräuchte?

Ranuk fühlte sich immer noch unverschämt wegen der
Augenbindensache. Aber auf der anderen Seite war es auch lustig
und ausreichend surreal, dass das Unverschämtheitsgefühl sich
in folgedessen auch unreal anfühlte und nicht so bohrte.

Ranuk wartete ein wenig ab und schritt dann die Treppe
hinunter. Rie kam gerade am Fuß der Treppe an, als die Spühlung
ging. Das war ein zu früher Moment zum Hinabsteigen gewesen. Nun
wartete rie mit dem seltsamen Gefühl, zu beobachten, auch
noch ab, bis Emre sich die Hände gewaschen hatte und den
Raum verlassen würde.

Bis sich die Tür öffnete, wanderte
Ranuks Blick verstohlen auf den Spiegel im Flur.
'Gut Gemacht Pizza' stand eilig geschrieben darauf. Kryptisch.

Emre hatte hellbraune Haut, trug eine schwarze Arbeitslatzhose mit
neonfarbenen Reißverschlüssen an jeder der vielen Taschen, und darunter
ein ärmelloses Oberteil. Das wäre Ranuk zu kalt bei dem Wetter. Aber
dass Emre nicht so leicht fror, war ihs gestern schon aufgefallen.

Emre wendete sich Richtung Ausgang, mit den nackten Füßen die
Aufkleber auf dem Boden ertastend, die Ranuk zur Orientierung
dahingeklebt hatte. Dass Emre Ranuk nicht beachtete, lag natürlich
daran, dass Ranuk keinen Ton von sich gab. Und einen Ton von
sich zu geben, war echt nicht so einfach. Welchen denn
bitte?

Ranuk räusperte sich.

Emre zuckte zusammen, stolperte, und ging in die Hocke, um sich
aufzufangen. Das Manöver war natürlich dazu da, nichts umzuwerfen,
während Emre gar nicht wusste, was da sein könnte. Es klappte. Emre
berührte nicht einmal den großen Blumentopf mit der vertrockneten
Pflanze darin. Es wäre auch nicht schade darum gewesen, überlegte
Ranuk.

"Guten Morgen!", grüßte Emre, ohne sich umzudrehen.

"Mittag eher", korrigierte Ranuk.

"Guten Mittag!", grüßte Emre, als wäre der Dialog vorher
nicht gewesen.

"Ich wollte Brötchen zum Frühstück anbieten, aber ich habe
sie alle aufgegessen, weil ich Hunger hatte", erklärte
Ranuk. "Ich wollte welche übrig lassen, aber wenn ich spät
frühstücke, frühstücke ich viel. Ich könnte jetzt höchstens
Pizza anbieten. Willst du Pizza?" Die Idee des Geists
hatte sich einfach in ihs festgesetzt und verselbstständigt.

"Pizza zum Spätstück?" Emre klang durchaus angetan. "Ich
habe ein paar Reste von meinem Abendbrot gestern gefrühstückt. Wobei,
es eher Abendgenüdel als Abendbrot war. Wenn
dir danach ist, mit mir noch zu spätstücken, würde ich aber
zu ein zwei Stücken Pizza nicht 'nein' sagen."

"Ein oder zwei Spätstücke Pizza also", murmelte
Ranuk. "Dann würde ich eine Pizza
in den Ofen schieben und", Ranuk unterbrach sich, "ich
hätte gesagt, wir setzen uns draußen hin. Ich sitze sehr selten
mal mit einem Liegestuhl in der Schneise, aber ich weiß nicht, ob
dir das genug ist."

"Die Schneise reicht für zwei." Emre sah sich immer noch nicht
um. Also, Emre drehte sich immer noch nicht um. Selbst wenn Emre es getan
hätte, hätte Emre ja nichts gesehen. "Aber wenn die Pizza eine
halbe Stunde im Ofen weilen muss und du einen Moment Zeit hast, mir
zu sagen, wo genau, habe ich vielleicht bis dahin schon ein Stück
Garten freigesenst. Vorausgesetzt, ich bin eine naturtalentige
Sensenperson."

---

Damit Emre mehr Zeit für die Sache hätte und damit Ranuk bei der Sache
weniger in Verlegenheit käme, zugucken zu müssen, folgte Ranuk Emre
zuerst in den Garten. Dort nahm Emre die Augenbinde ab. Vom Garten aus
konnte zwar ins Haus gesehen werden, aber nur in Bereiche ohne Spiegel. Chaos
gab es trotzdem.

Es hatte mal eine Tischecke gegeben, wo immer noch ein alter Holztisch stand, von
dem Ranuk sich nicht sicher war, ob er noch zu retten war. Er war
aufgequollen und eingewuchert. Die Stühle, die zu Zeiten der
Nutzung durch ihse Eltern darum herum gestanden
hatten, waren im Schuppen gewesen und lagen nun im hohen Gras
daneben. Da der Schuppen nicht dicht war, hatten auch jene gelitten. Aber
da nur die Sitzflächen aus Holz, die Verstrebungen aber aus Metall waren, sollten
sie noch brauchbar halten. Vielleicht. Rost war ja auch eine Sache, die
existierte und ihr Ding tat.

Emre hatte das Prinzip gut verstanden, dass der Garten hinterher
anders aufgeteilt sein sollte als vorher, und schlug einen anderen
Ort vor, um eine Laube für eine Sitzgelegenheit zu bauen, wo der Tisch
auch schon zuvor, noch ohne Laube, gut sein könnte. Sie
gingen dazu einen Kreis ab, in dessen Inneren das hohe
Gras, das sie fast überragte, weichen sollte. Dabei traten sie
ein paar der Halme platt, die meisten bogen sie aber im Wesentlichen
vorübergehend auseinander, sodass eine Vorgabe
sichtbar wurde. Das hohe Gras war eigentlich auch schön. Das
war noch ein Grund, warum sich Ranuk nicht selbst daran gemacht
hatte, es zu entfernen. Teile davon sollten vielleicht stehen
bleiben. Aber einen Ort zum Draußensitzen vermisste rie eben
schon.

"Es ist so hoch.", murmelte rie.

Emre schaute in den Himmel. "Es ist noch nicht am Himmel
angestoßen. Da ist noch Platz."

Ranuk folgte dem Blick gen Himmel und lächelte unwillkührlich. "Emre", sagte
rie, "wenn du auch nur ein bisschen von deinem dichterischen Humor
in deine Anzeige getan hättest, hättest du überall landen können."

"Aber wo wäre ich besser aufgehoben, als bei der Zielgruppe für
meinen dichterischen Humor?"

Diese Schlagfertigkeit! Ranuk blickte zurück in Emres Gesicht, das
rie eher im Profil sah, weil Emres Blick immer noch gen Himmel
gerichtet war. Die Augenbrauen waren fast schwarz und dicht. Die Haare ebenso
schwarz, dicht und gelockt, nicht sehr fein, sondern so, dass sich
die Strähnen in ein bis zwei Umdrehungen geringelt um den Kopf
verteilten. Es war eine schöne Frisur, fand Ranuk. Sie erinnerte
an Kinder- und Jugendfilme aus Ranuks Jugend, also so 50 bis 60 Jahre
alte Streifen.

"Und du meinst nicht, dass du eine bessere Unterkunft hättest finden
können, wo dein Humor auch angekommen wäre?", fragte Ranuk trotzdem
unsicher.

Emre schüttelte den Kopf und sah Ranuk gelassen an. "Ich halte es
nicht für ausgeschlossen, aber schon für sehr unwahrscheinlich. Du
hast einen malerisch schönen Garten. Du wohnst weit weg von allem, was
mich stresst. Ich habe mich seit zwei Jahren nicht so entspannt
gefühlt. Da fühlt sich die Toilettensache fast mehr als Abenteuer
oder angenehme Herausforderung als als Einschränkung an."

Ranuk lächelte unwillkührlich. Rie fiel dazu allerdings nichts zu
sagen ein. Also verabschiedete rie sich: "Ich kümmere mich mal
um die olle Tiefkühlpizza."

---

Das Gras, das Emre stehen ließ, wirkte gelblich, weil dort
lange keine Sonne mehr hingekommen war. Das hohe abgeschnittene
Gras lag nun lang auf dem Boden darum herum und trocknete. Es war ein
seltsames Provisorium, auf das Emre den Tisch und Stühle platziert
hatte, aber eines, das sich endlich nach Veränderung anfühlte.

Sie teilten sich die Pizza wie abgesprochen und sprachen beim
Essen wenig. Ranuk konnte sich nicht daran erinnern, wann rie
zuletzt so bewusst eine Pizza gegessen hatte. Tiefkühlpizza war
nun nicht die ausgewogenste Mahlzeit. Zu Ranuks Bedauern bestand
ihs Speiseplan eher aus Fertiggerichten. Weil rie selbst
dauernd zu fertig für ein anderes Gericht war, und weil bei
dem Chaos in der Küche nur schwer zu kochen war. Oft türmte
sich Geschirr auf dem Herd, weil es in der kleinen Küche nirgends
sonst Platz hatte und weil Ranuk keine Spühlmaschine hatte. Immerhin
schaffte Ranuk etwa zweimal in der Woche zu spülen. Und
entsprechend war der Herd zweimal in der Woche benutzbar. Aber
da kein Platz zum Schnibbeln da war, machte Ranuk auch dann
nur eine Fertiggemüsepfanne oder so etwas.

Nun draußen essen zu können, war jedenfalls eine sehr großartige
Verbesserung der Lage. Und die Gesellschaft irgendwie auch. Ranuk
aß ja nicht ohne Grund vorm Rechner chattend. Eigentlich aß
rie gern mit Gesellschaft zusammen. Besonders mit welcher, die
rie nicht ins Haus lassen musste.

---

Am Nachmittag malten Emre und Ranuk gemeinsam auf Schmierpapier
einen Plan, wie der Garten ungefähr am Ende aussehen sollte, und
schrieben eine Liste, die Emre schrittweise abarbeiten wollte. Emre
neigte zum Perfektionieren, aber ließ sich von Ranuks Entscheidungen
davon abbringen, stundenlang über Optimierung und Priorisierung
nachzudenken. Emre bedankte sich sogar dafür, kannte das
Problem von sich schon.

Es dauerte bis zum frühen Abend, -- der natürlich kaum dunkler war, als
der übrige Tag. Aber erschöpft waren sie schließlich beide.

Überrascht stellte Ranuk fest, dass rie sogar zu müde war, noch
Escube zu zocken, also zog rie sich um und verkroch sich ins
Bett. Im Licht, das durch das Fenster hereinfiel,
glitzernd stand in großen, blutigen Buchstaben das Wort 'Käsekuchen'
auf dem Schrankspiegel. Kleine Tröpfchen hatten Pfade von den Unterkanten
der großen Buchstaben den Spiegel hinabgefunden. Leute, die immer was zu
nölen hatten, hätten hier wahrscheinlich das Marketing kritisiert: Die
Aufmachung erweckte Erwartungen, die stark vom Inhalt abwichen. Aber
die Zeiten, dass mit Blut geschriebene riesige Wörter Ranuk gegruselt
hätten, waren längst vorbei. Oder hatte sich Ranuk überhaupt je
gegruselt? Vielleicht nicht einmal beim ersten Mal.

'Käsekuchen' war eine sich wiederholende Botschaft. Sie stand dort
jedes Mal, wenn Ranuk die Lust auf Käsekuchen besonders nicht verdrängen
konnte. Ranuk liebte Käsekuchen einfach. Aber er war sehr
kompliziert zu machen. Rie war der Aufforderung, wenn es denn eine
war, nur einmal am Anfang nachgekommen. Rie hatte sogar ein
Stück neben einen Spiegel gestellt, um mit dem Geist zu teilen. Aber
es war auch nicht verschwunden, als Ranuk eine Weile woanders
gewesen war.

So groß und überzeugt wie nun war das Wort allerdings selten
buchstabiert gewesen.
