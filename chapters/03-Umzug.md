Umzug
=====

\Beitext{Emre}

Das Wort 'Umzug' hatte auch zwei U. Zuumuutung hatte zwei U mehr als
üblich. Zwei U, die eine Person für Emre mit nur einem Wort
sympathatisch gemacht hatten. Das Wort Umzug dagegen war trotz der
zwei U eher zwiespältig. Das Ergebnis eines Umzugs war meistens irgendwann
gut, aber der Umzug selbst barg so eine enorme Last, dass das Ergebnis nicht
mit dieser zusammen in eine einzelne Vorstellung passte. Vor allem nicht
in einem ausgebrannten Hirn, wie Emre eines hatte.

Emre fühlte im Moment, wie jeder Tag sich ein bisschen leichter anfühlte als
der vorherige. Jeder Tag, an dem Emre nicht mehr arbeiten musste, weil
Emre sich nun mit dem BGE nicht um einen weiteren Job in dem
Unternehmen gekümmert hatte, nachdem der befristete Vertrag ausgelaufen
war. Emres Arbeit war solide gewesen, aber nie so herausragend, dass
Emre eine feste Anstellung hätte bekommen können. Es waren über nun mehr
acht Jahre eine befristete Stelle nach der nächsten gewesen, wenn es
hochkam mal für ein Jahr, aber sonst auch oft nur für zwei bis drei
Monate. Stellen, für die Gelder beantragt werden mussten, was Emre neben
der Arbeitszeit auch hatte bewerkstelligen müssen. Stellen mit ständig
leicht unterschiedlichem Einkommen, weil sie zwischen Drittelzeit und
Halbzeit schwankten, was Steuererklärungsgedöns schwieriger machte. Emre
arbeitete allerdings immer voll darauf, das war auch bei dem
verlangten Pensum nicht anders möglich und üblich. Und
all das war immer noch einfacher gewesen, als offiziell krank zu sein,
was Emre eigentlich schon seit mindestens drei Jahren war.

Emre hatte nun seit einem Monat keine Arbeit mehr und über die Hälfte
des Monats verschlafen, -- also nicht am Stück, sondern jeweils
die halbe Nacht und den halben Tag --, weil einfach nichts mehr ging. Es
fühlte sich wie ein riesiger Overload an, ein riesiges Zuviel, das
nun mental abgearbeitet werden musste. Allmählich ließ das Gefühl
nach, irgendwas zu müssen, um überleben zu dürfen, wenn Emre gegen
Mittag aufwachte. Und bei dem Gedanken kamen Emre Tränen der
Erleichterung. Tränen. Interessante Sache, so eine gefühlige
Körperreaktion. Dass sich das wieder einschaltete. Das hatte Emre
seit Monaten nicht gehabt, oder waren es Jahre? Emre hatte nicht einmal Kraft für
Wut gehabt.

Emre wusste, dass es viele wütende Leute gab, vor allem Marginalisierte, die
das BGE an sich zwar toll fanden, die Umsetzung aber kritisch sahen. Personen, denen
Emre vertraute, dass sie sich damit irgendwie auseinandergesetzt hatten. Wenn
diese sagten, dass das als kapitalistischer Trick gedacht war, um zu
zeigen, dass das eh nicht klappen würde mit dem BGE, dann war da vermutlich
was dran. Emre hätte sich früher in das Thema gestürzt, Leute informiert, versucht,
sinnvollen Aktivisimus zu machen und wäre wütend gewesen. Aber dazu fehlte
Emre jede Energie. Es war egal, was ein sinnvolles Verhalten gewesen
wäre, mit dem BGE umzugehen. Wenn das System wegen des BGEs kollabieren
würde, weil Leute, die krank waren, durch das BGE nun nicht mehr weiter
arbeiteten, dann war dieses Kollabieren vielleicht auch irgendwie notwendig
oder früher oder später eine Unausweichlichkeit. Aber wahrscheinlich
war das zu einfach gedacht.

Emre jedenfalls las absichtlich nichts zum Thema, unterhielt sich nicht mit Leuten
darüber, versuchte den Gedankenapparat, der in alter Manier automatisch
versuchte, sich damit auseinanderzusetzen, welche Auswirkungen Emres
Verhalten hätte, abzuschalten, und sich darauf zu fokussieren, gesund
zu werden oder zumindest gesünder. Arbeit im Freien auf dem Land, wo es
nicht so warm war. Möglichst ohne Zeitdruck -- und das war der
komplizierte Punkt. Dadurch konnte Emre nicht einfach Feldarbeit oder
sowas machen, weil es da feste Zeitpläne gab und Leute üblicherweise auch
eher voll arbeiteten. Oder nun mit BGE nicht mehr? Wer weiß.

Auf Emres Anzeige hatten sich jedenfalls nur wenige Leute gemeldet, und
Ranuk war mit Abstand am interessantesten und sympathischsten und
hatte die besten Standortfaktoren. Norden war wichtiger
als die Sache mit der Toilette.

In den vergangenen Tagen hatten sie sich gemeinsam Konzepte für den Garten
ausgedacht. Ranuk hatte Fotos geschickt und Emre hatte Überlegungen
angestellt, wie er verändert werden könnte, recherchiert und Vorschläge
gemacht. Ranuk hatte sich dann Dinge erklären lassen und ausgesucht. Ranuk
hatte praktischerweise weniger Entscheidungsschwierigkeiten als
Emre. Emre hätte sich mehr Zeit zum Abwägen genommen. Ranuk war
mehr so: Hauptsache anders, nicht zu kompliziert, halbwegs
pflegeleicht und irgendwie hübsch. Trotzdem machten sie sich auch
ein paar Gedanken über Insektenfreundlichkeit.

Die Recherche hatte Emre nicht allein gemacht, sondern mit Ranuks
Einverständnis die Bilder an Emres Herzwesen Enne mit
viel Ahnung von Pflanzen geschickt, weil Emre
selbst eigentlich gar keinen so furchtbar grünen Daumen hatte. Enne
hatte dann erklärt, welche Ansätze wie einfach umzusetzen und hinterher
wie pflegeleicht wären. Emre hatte dann herausgefunden, welches Werk-
und Gartenzeug notwendig wäre und sich bei Ranuk erkundigt, was da war. Es
war eine Menge da. Aber eine Sense gab es nicht.

Und so kam es, dass Emre an einem heißen Sommertag in einen schwarzen
Mantel gehüllt, einen schweren Wanderrucksack (in den besagter Mantel einfach
nicht mehr gepasst hatte) auf dem Rücken und einer Sense in der Hand übermüdet
im überfüllten öffentlichen Personenverkehr stand und von früh bis spät
gen Norden fuhr. Es fühlte sich seltsam an. Nicht unbedingt gut. Emre
befürchtete Verletzungsgefahr. Aber es fühlte sich auch albern und surreal an, sodass
sich ein ungewolltes Grinsen in Emres Gesicht verklemmte, das
vermutlich nicht nur einen gruseligen Aspekt zur Ästhetik hinzufügte, sondern
auch auf Dauer Schmerzen in den Gesichtsmuskeln verursachte.

Wenn Emre für den folgenden Tag keinen Plan hatte, schlief Emre durchaus
gut und lang, und schaffte es oft am dann anbrechenden Tag für Stunden nicht aus
dem Bett, weil es nicht passieren musste und doch auch gemütlich im
Bett war und Emre dauererschöpft. Wenn am nächsten Tag noch vor Sonnenaufgang
der Zug abfahren würde, dann schaffte Emre es pünktlich dorthin, aber
konnte die Nacht kaum sinnvoll oder entspannt schlafen, hatte Panik und
in Halbschlafträumen lief nichts so, wie es sollte. Daher kam die
Übermüdung.

Emre fuhr mit dem Zug von Fork nach Geesthaven zur Fähre nach Fjärsholm und
bekam jene gerade so, weil die zwei Stunden Verspätung, die Emre vorsichtshalber
einberechnet hatte, nur ausgeschöpft und nicht überschritten
wurden. Auf der Fähre fühlte sich eine Sense
irgendwie weniger gefährlich und stilvoller an. Der Wind zerrte am Mantel, der
bei der Überfahrt sinnvoller an- als ausgezogen war. Eigentlich war beides
falsch. Unter dem Mantel trug Emre bloß ein ärmelloses Oberteil, das mit
dem Wind auf dem Wasser vielleicht angenehm kühl gewesen wäre, aber
Sonnenbrand wohl garantiert hätte. Der
Mantel schützte also vor Sonnenbrand und fühlte sich nicht ganz so schlimm an wie
in den Zügen. Emre hätte wahrscheinlich etwas Dünnes, Langärmliges auspacken sollen, aber war
zu erschöpft, das Gepäck zu durchwühlen. Allein das Öffnen des Rucksacks wäre
zu viel gewesen. Eincremen wäre noch anstrengender gewesen. BurnOut eben.

Ab Fjärsholm fuhren nach
Høppla vor allem kleine Zuckelzüge, aber immerhin waren sie sehr pünktlich. Skandern
hatte vor der Kontinentisierung Maerdhas ein durchaus zuverlässiges öffentliches
Verkehrssystem gehabt, das auch nicht sehr unter der Vereinheitlichung gelitten
hatte. Vielleicht, weil die Bevölkerungsdichte hier so viel geringer war und eben
noch die selben Leute arbeiteten wie zuvor.

Als Emre in Høppla ausstieg, war der schwarze Mantel zwar immer noch zu
warm, aber fühlte sich nicht mehr so an, als würde er Emre gefährden. Emre
hatte zwischendurch Angst gehabt, eher in einem Krankenhaus anzukommen als
in Høppla. Der einzige Grund, warum Emre hier noch einigermaßen
gerade stand, war der Einfluss von einer Ipro-Tablette gegen Kopfschmerzen und
für Entspannung gewesen. Emre tat alles weh, besonders die Schultern, auf
denen der Rucksack trotz Beckengurt zog, und konnte sich eignetlich nicht
vorstellen, vorm Schlafen noch eine Hütte ausräumen zu müssen. Aber was musste,
das musste wohl.

Als sich der Bahnhof allmählich leerte, wurde klarer, wer Gaby sein
musste. Gaby trug ein blaues Scientists-For-Future-T-Shirt mit abgetrennten
und sauber umgenähten Ärmeln über einer langärmligen Bluse und eine
ausgewaschene Jeans mit ein paar Flicken. Das T-Shirt löste
in Emre einen gewissen Frust aus, den Emre schnell verdrängte. Emre
hatte auch mal zu den Scientists For Future gehört, und das
war leider mitursächlich für den BurnOut gewesen. Stattdessen
konzentrierte Emre sich auf die Flicken: Es waren Segelboote,
Leuchttürme und ein Deicheinhorn darunter.

"Moin!", grüßte Gaby, ohne die Hand anzubieten.

Emre versuchte ein Lächeln und grüßte vorsichtig mit einem "Hi"
zurück. "Ich bin Emre."

"Schicker Mantel." Gaby hielt sich nicht lang damit auf, Smalltalk
zu versuchen, sondern führte Emre zu einem kleinen Auto, in das
Emres Gepäck trotzdem brauchbar Platz hatte.

Emre fühlte sich nervös dabei, die Sense darin zu verstauen, und dass
Gaby irgendwelche Sprüche in Richtung Gevatter Tod machte, half dabei
auch nicht.

"Oder müsste ich bei dir Gemutter Tod sagen?", fragte Gaby. "Hm?"

Emre ging nicht darauf ein und versuchte vergeblich, das
Thema zu wechseln. Vielleicht
war es aber auch Gaby nicht so sehr zu verübeln, dass Gaby
Emres Einwürfe wie 'Oh!', 'So vielleicht?', 'Uff' und 'Aua' nicht
als Wink mit dem Zaunpfahl auffasste, über etwas anderes
zu philosophieren.

Als sie bald darauf fuhren, war es dann endlich abgehakt. Gaby
hatte einen sicheren, angenehmen Fahrstil, fand Emre.

"Und Ranuk wird dich nicht ins Haus lassen, habe ich das
richtig verstanden?", fragte Gaby.

"So ungefähr", antworte Emre. Es gab da ja diese Ausnahme mit
den verbundenen Augen. Aber das war eine Sache zwischen Ranuk
und Emre.

"Wie fühlst du dich damit?", fragte Gaby. "Ist 'du' okay?"

Gaby hatte hellbraune Haut und, wie im hohen Norden
häufiger, verbargen sich ihre Augen fast hinter vielen Fältchen,
als würden sie ständig ins Licht blinzeln, aber wirkten
dadurch nicht unentspannt.

"Duzen ist in Ordnung.", antwortete Emre. "Und ich bekomme
ein eigenes, kleines Haus, wieso sollte ich mich beschweren?"

"Du kannst jedenfalls gern mal bei mir übernachten, wenn es
dir zu viel wird. Ich weiß ja nicht, wie lange du bleibst", versicherte
Gaby.

---

Emre genoss diesen kurzen Augenblick von Ruhe, als sie am Zielort
ausstiegen. Den Mantel inzwischen über den Arm gehängt fühlte
Emre die angenehme Kühle des hereinbrechenden Abends und eine
Portion böigen Wind auf der Haut, die Einsatz des Gleichgewichtsinns
erforderte. Es roch nach feuchten Pflanzen und irgendwie naturnäher
als alles, was Emre in den letzten Wochen erlebt hatte. Das war
angenehm und fühlte sich sehr erleichternd an.

Gaby klingelte, klingelte nach einer kurzen Wartezeit noch einmal
und zückte schließlich, als immer noch niemand reagierte, das
Handy, um anzurufen. Es ging immerhin rasch jemand ran und das
Telefonat dauerte nicht lang.

Ranuk kam wenige Augenblicke später eine Art Weg entlang, der um das Haus
herumführte. Ranuk hatte weißes Haar, das vielleicht mal blond
gewesen war, in einen zweckmäßigen Zopf zusammengefasst. Ranuks
Gang war eher gebückt, als hätte Ranuk Schmerzen. Und das schwere
Atmen sprach auch ein wenig dafür.

"Ich habe es nicht über mich gebracht und angefangen, Zeug aus der
Hütte zu werfen, damit du schneller schlafen kannst, und dabei
die Zeit vergessen", erklärte Ranuk.

"Das ist lieb." Emre fiel nichts Besseres zu sagen ein und bereute
das ein bisschen. Es fühlte sich furchtbar erleichternd an. Emre
hätte gern das ganze Gefühl vermittelt, und das passte nicht
in diesen kleinen Satz. "Wirklich lieb!"

"Ich lasse euch zwei mal allein. Du bist sicher müde", beschloss
Gaby. "Wenn ihr mich braucht, ruft mich an und ich komme."

Emre fand Gaby durchaus nett und versuchte, sich freundlich zu
verabschieden. Dann folgte Emre Ranuk in den Garten. Er roch
gut. An manchen Stellen roch er etwas faulig, aber das empfand
Emre nicht als negativen Geruch.

Ansonsten war alles wie erwartet, nur dass neben der Hütte nun
Fahrräder im Gras lagen, darunter ein Tandem, sowie ein
Rasenmäher, Gartenmöbel, anderes Gestänge und Gedöns.

"Ich bin noch nicht fertig geworden", sagte Ranuk.

"Dazu bin ich ja auch hier", beschwichtgte Emre.

"Und ich habe so eine dicke, blaue Luftmatratze gefunden", fuhr
Ranuk fort, ohne auf Emre einzugehen. "Die ist vielleicht angenehmer
als dieses dünne Reiseding, von dem du erzählt hast. Ich weiß nur
nicht, ob sie Löcher hat."

---

Ranuk zeigte Lichtschalter und Zeltplane und erklärte, dass Emre
einfach den Rest rausschmeißen dürfe und sich wann anders dann
um das so zusätzlich entstandene Gartenchaos kümmern könnte. Emre möge sich erstmal
um sich kümmern. Außerdem würde es laut recht zuverlässigem
Wetterbericht in der Nacht nicht regnen, sodass das provisorische
Dachflicken auch bis morgen Zeit hätte.

Ranuk blieb nicht lange. Nachdem alle ersten Fragen beantwortet
waren und Ranuk Emre einmal zum Klo geführt hatte, zog
sich Ranuk zurück.

Emre räumte noch so viel Kram aus der Hütte, bis die große Matratze
passte. Das war der anstrengendste Teil, weil es eine klapperige
Arbeit war, bei der sich viel verhakte, und trotz schlechter
Konzentration Dinge mehr klappen als klappern mussten. Dann legte
sich Emre auf den kalten, staubigen Holzboden der Hütte und
bließ langsam die Luftmatratze auf. Mit dem Mund. Tiefe Atemzüge. Die
Luft der Hütte einatmend, den Boden bei der Bewegung erfühlend, und
langsam in die Matratze ausatmend, die ein wenig nach
Plastik roch und schmeckte. Sich darauf freuend, sich selbst
gleich daraufzulegen, und wenn es nur für ein paar Minuten war, falls
sie undicht wäre.

Es war ein unbeschreiblich schönes Gefühl, nach der ganzen
Fahrt, nach dem ganzen Herumgeleiste, weit weg im Norden, wo Emres
Körper nicht schmolz, auf einer Matratze zu liegen und fische Luft
zu atmen. Emre wäre fast einfach eingeschlafen. Für den Moment glücklich und
zufrieden. Aber Emres Magen beschwerte sich, zu wenig gegessen
zu haben.

Ranuk hatte eine Kochplatte zur Verfügung gestellt, eine recht
neue sogar, und ein paar Töpfe und etwas Geschirr. Emre hatte für
die ersten Tage ein paar Lebensmittel eingepackt. Heute war
Emres Lieblings-Ankomm-Essen dran. Eine lange nicht mehr ausgeführte
Tradition: es war nicht das erste Mal, dass Emre eine lange Anreise
zu so etwas wie Urlaub machte, aber das letzte Mal war vor vielen Jahren gewesen.

Emre setzte einen Topf mit Wasser auf, in den bald die langen, breiten
Nudeln kommen würden, und in einem zweiten bereitete Emre rot-weiße
Soße mit angedünsteten Topraten vor. Dabei stand die Tür der Hütte
offen, damit sich die Feuchtigkeit und der Geruch verziehen konnten, und
kalte, grüne Abendluft strömte aus dem Garten herein.

Wie sehr hatte Emre diese Ruhe und Selbstbestimmung vermisst.
