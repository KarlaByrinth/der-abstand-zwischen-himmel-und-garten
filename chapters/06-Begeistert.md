Begeistert
==========

\Beitext{Emre}

Emre fühlte sich so entspannt wie lange nicht mehr. Alles tat
weh. Angenehm weh. Von der Arbeit der letzten Tage. Der Geruch
von Gras drang durch das offene Fenster in die Hütte. Emre
lag entspannt, mit den meisten Teilen des Körpers direkt auf
dem Boden, weil die Matratze um diese Uhrzeit allmählich leer
war. Emre pustete sie jeden Abend wieder auf. Und hatte
bislang nicht Bescheid gegeben, dass es kein Flickzeug
gab. Ranuk hatte ja ohnehin ein schlechtes
Gewissen wegen der angeblich mangelnden Besuchsfreundschaft. Aber
Emre mangelte es an nichts.

Als Emre bewusst darauf lauschend Ranuks erste Rödeleien
des Tages vernahm, stand Emre auf. Gemütlich. Ranuk
war eigentlich nicht laut. Aber irgendwie hatte sich, ungefähr seit
Emre hier war, eine Tradition entwickelt, dass sie draußen
gemeinsam frühstückten, wann immer das Wetter es auch nur
einigermaßen zuließ. Das reduzierte das Gewissen bei
Ranuk, und Emre war die Tradition durchaus recht. Sie
garantierte, dass Emre tatsächlich irgendwann aufstand.

Sie frühstückten querbeet, was da war. Manchmal Rundlinge, manchmal
Müsli, gelegentlich Reis, häufig Reste von gestern, und heute: Käsekuchen.

"Aber wie erzähle ich sowas bloß?", murmelte Ranuk, als sie,
Besteck und Geschirr sinnvoll auf dem und
um den Tisch verteilt hatten, die Ziege dabei berücksichtigend, den
Kuchen in der Mitte.

Emre war nicht sicher, ob es zu sich selbst gemurmelt oder an
Emre gerichtet war. Aber fragte vorsichtshalber mal trotzdem: "Was
denn?"

"Dass in meinem Haus ein Geist lebt." Es war die selbe abwesende
Sprechart, bei der Emre sich unsicher war, ob Ranuk bewusst
war, es ausgesprochen zu haben, oder nicht.

"Ungefähr so: In meinem Haus lebt ein Geist", schlug Emre vor.

Ranuk blickte Emre plötzlich etwas wacher an. "Woher weißt du?"

Emre antwortete nicht und blickte zurück.

"Oh", machte Ranuk.

"Dir war wirklich nicht bewusst, dass du das gerade laut sagst", stellte
Emre fest.

Ranuk lächelte, aber es wirkte auch ein bisschen entgeistert. Oder
so. "Es war mir schon bewusst, aber irgendwie, ich weiß auch nicht, hatte ich den
Eindruck, ich rede mit einem anderen du? Ich kann das gerade nicht
besser beschreiben."

"Ich glaube, ich verstehe", sagte Emre. "Es klingt ein bisschen nach
Derealisierung oder so etwas. Das hatte ich viel, als ich sehr gestresst
war."

"Bist du es nicht mehr?", fragte Ranuk.

Emre stand vom Tisch auf, ging zum Zaun und pflückte sich eine
Pusteblume. Ate betrachtete das Vorgehen neugierig vom Tisch. Ranuk
hingegen beschäftigte sich damit, den Käsekuchen anzuschneiden. Sie
hatten zusammengelegt, um ihn sich
zu kaufen, und Gaby hatte ihn mit dem Wocheneinkauf mitgebracht. Emre
hatte überlegt, so etwas allmählich mit dem Rad zu erledigen, aber
bis Høppla war es schon eine Ecke. Hier in der Gegend verkauften
vor allem kleine Landwirtschaftsbetriebe in Verkaufshütten angebaute
Lebensmittel, die hilfreich wären, wäre Emre in die Küche gelassen
worden oder hätte Ranuk Energie gehabt, zu kochen.

Emre setzte sich wieder an den Tisch und betrachtete die flauschige
Blume. Und pustete nach einer Weile sehr behutsam die Fliegeteile
weg, sah ihnen nach, wie sie durch den Wind davon getragen wurden. "Seit
ich hier bin, geht es mir viel besser", erklärte
Emre. "Ich kann atmen. Ich kann wieder denken. In Fork war es
zu heiß zum Denken. Ich habe so drei Stunden am Tag denken
können. Das ist so erleichternd, das wieder meistens zu können!"

"So, wie du hier rumläufst, wäre mir zu kalt zum Denken", kommentierte
Ranuk.

"Ich werde mich schon irgendwann wärmer anziehen", mutmaßte
Emre. "Im Moment habe ich den Eindruck, Überhitzung von mindestens
zwei Jahren ausdampfen zu müssen. Wie nach einem Sauna-Besuch in
Kleidung, nur halt nicht so kurz."

"Verstehe."

Sie schwiegen einige Momente. Ranuk war nicht gut mit Worten, hatte
Emre den Eindruck. Aber irgendwie vermittelte Ranuk Emre doch
ein Gefühl, wertvoll zu sein. Und einfach sein zu dürfen. Es gab
so viel Stress, den Emre sonst mit Leuten hatte; mit Ranuk nicht.

"Wenn du schon überlegst, ob ich derealisiere, glaubst du
dann eh nicht an den Geist?", fragte Ranuk und kam so zum
Thema zurück.

"Warum sollte ich nicht?", fragte Emre. "Ich meine, erst einmal spielt
es keine Rolle, ob der Geist existiert oder nicht, um mit
dir ernsthaft über das Thema zu reden. Aber dann ist die Option, dass
er existiert auch die lohnenswertere, interessantere, anzunehmen."

Ranuk grinste. "Klingt schon so, als würdest du nicht wirklich dran
glauben, sondern nur für Unterhaltungen mit mir als Grundlage annehmen, dass
er existiert, weil es das Gespräch ergiebiger machen könnte." Ranuk löste ein
Pusteblumendings aus dem Kuchen, das sich dort verfangen hatte, und
bließ es in die Luft. "Damit kann ich trotzdem gut leben."

"Ich entscheide mich nicht so schnell, ob ich an etwas glaube
oder nicht.", hielt Emre fest.

"Oh, das gefällt mir!", sagte Ranuk.

In Emre bildeten sich Fragen über Fragen. Deshalb stellte Emre
zunächst überhaupt keine. Laut nicht. Innerlich fragte sich Emre: War
das der Grund, warum Emre nur mit geschlossenen Augen ins
Haus durfte? Warum erzählte Ranuk davon? Weil Emre mit dem
Geist in Kontakt treten sollte? War Ranuk wichtig, dass
Emre zügig daran glaubte? Wollte Ranuk beweisen, dass es den
Geist gab? Sollte Emre
eher die Gesprächsrichtung Beweise einschlagen, oder
die Gesprächsrichtung, was es für ein Geist war und was
er tat? Emre überlegte, dass letzteres schöner war, akzeptierender.

"Was denkst du darüber? Oder deswegen von mir?", unerbrach
Ranuk Emres Gedanken.

"Ich denke deswegen jetzt nicht anders von dir", antwortete
Emre wahrheitsgemäß. "Ich kann mir vorstellen, dass es
einiges an Mut kostet, von einem Geist im Haus zu erzählen. Und
mache mir Gedanken darüber, was du jetzt brauchst. Und wie ich
dir klar machen kann, dass ich mich nicht über dich lustig machen
oder mich über dich erheben werde oder sowas."

Emre blickte auf, als von Ranuks Seite ein kurzes Aufschluchzen
im Atem zu hören war. Da waren außerdem Tränen in den Augen, um
die herum sich in der dadurch schönen Haut viele kleine Fältchen
gesammelt hatten. Nicht nur Lachfalten, sondern auch einfach welche, die
in alter Haut entstanden.

Ranuk wischte die Tränen weg. "Danke. Ich weiß auch nicht, Akzeptanz
ist halt auch immer noch was Besonderes."

"Ich weiß", sagte Emre traurig. Und dann: "Wie ist dein Verhältnis
zum Geist? Bist du zufrieden mit der Anwesenheit, oder ist
dey eher unnett zu dir?"

"Oh, du verwendest ein Neopronomen", stellte Ranuk fest.

Emre ging nicht darauf ein. Es war ein Thema. Ein großes, über
das Emre im Leben gefühlt schon viel zu viel geredet hatte. Vielleicht
wäre es mit Ranuk leichter. Aber Emre war müde. Und war eigentlich
ganz froh, dass sie den Themenkomplex noch nicht angefasst hatten. Dass
er mal nicht wichtig war. Wobei Emre positiv aufgefallen war, dass
Ranuk Emre nicht einfach irgendwie genderte oder
geschlechtspezifische Kommentare machte. Sehr positiv. Vielleicht
war es das erste Mal in Emres Leben, dass so lange Zeit am Stück
Geschlecht wirklich gar keine Rolle gespielt hatte. Und das war
ganz schön.

Es fühlte sich an, als wäre der ganze Geschlechterkram auch
Teilschuld am BurnOut gewesen. Das sich Wehren gegen die
permanenten Zuweisungen von außen, was auf der einen Seite
so notwendig erschien und auf der anderen so sinnlos, weil
es ein Kampf gegen zu große Windmühlen war. Bei dem Emre
immer das Gefühl hatte, für andere eine zu große Last zu
sein. Dadurch dass Emre allein durch Emres Existenz das
Navigationsmodul für soziale Interaktion ungefähr aller Leute
über den Haufen warf, weil jenes Emre nicht vorsah.

"Ich versuche das auch mal", beschloss Ranuk. "Ich bin eigentlich
recht angetan von deren Anwesentheit. Dey schreibt Nachrichten
auf Spiegel, meistens motivierende, die mich ein bisschen in
den Hintern treten. Die letzte war 'Zeig dich'."

"Du bist also eher begeistert als entgeistert über den Geist", stellte
Emre fest.

Ranuk grinste. "Genau."

Emre lächelte mit. Und war sich inzwischen recht sicher, deswegen
die Augen für aufs Klo gehen verbunden haben zu sollen. "Ich
benutze für Entitäten, die ich nicht kenne, das Pronomen dey, und
für Gegenstände, die mehrere Pronomen haben, wie, -- ah, mir
fällt natürlich genau dann, wenn ich es brauche, keines ein --, das
Pronomen as. Und für Wörter, die wir aus anderen Sprachen
übernommen haben, möglichst das Pronomen aus jener Sprache. Also
zum Beispiel für Team it. Ich möchte damit Neopronomen auf eine Stufe mit anderen
Pronomen stellen, indem ich sie auch für Gegenstände benutzte", erklärte
Emre doch.

"Das finde ich cool!", begeisterte sich Ranuk. "Ich weiß, was du
meinst, mit Wörtern mit mehreren Pronomen. Mir begegnen dauernd
welche, wo ich sehr irritiert bin über die Pronomenwahl anderer
für das Wort, aber meinst du, dass mir gerade eins einfiele? Nope."

Die Ziege suchte sich diesen Moment aus, um laut zu meckern, mehrfach, einmal
zu rülpsen (obwohl sie gar keinen Käsekuchen gegessen hatte), und schließlich
über den Zaun davon zu hoppsen.

"Okay", stimmte Ranuk ihr zu.

"Was hat sie gesagt?", fragte Emre.

"Määh", sagte Ranuk. "Also, bisschen anders artikuliert. Aber ich
fand es sehr überzeugend irgendwie."

Emre grinste noch einmal. Und kam dann zum spannenden Thema zurück: "Warum
erzählst du mir vom Geist?"

"Also, neben der Aufforderung des Geistes selbst von wegen Zeigen, hat mich
Gaby ermahnt, dass ich dich mit offenen Augen ins Bad lassen soll", sagte
Ranuk, und bestätigte damit einige von Emres Gedanken. "Außerdem, und da
werde ich schon wieder zuumüütender: Ich hatte die Hoffnung, dass du mir
vielleicht auch bei meinem Chaos im Haus helfen magst. Mir
ist das sehr peinlich."

Emre nickte einfach. Und verkniff sich ein Lächeln, um Ranuk möglichst
das Gefühl zu geben, dass Emre Ranuk sehr ernst nahm. Aber innerlich
freute sich Emre unerhört darüber, dass es sich nach einem gewissen Vertrauen
anfühlte. "Sehr gern."

Ranuk blickte Emre eine Weile an und schließlich zurück auf den Tisch. "Ich
habe gar keinen Tee gemacht. Möchtest du Tee?"

"Auch gern!", sagte Emre, und fügte enthusiastisch hinzu: "Vampir!"

Ranuk blickte verwirrt zurück, was Emre Ranuk nicht verdenken konnte. "Der
Geist ist kein Vampir, würde ich behaupten. Woher weißt du von dem Blut?"

"Nein", widersprache Emre. "Es kann der oder das Vampir heißen. Weshalb ich
für Vampir das Pronomen 'as' nehme. Wobei ein Vampir sogar auch eine Entität
ist, weshalb ich zwischen 'as' und 'dey' wechsele.^[Anmerkung des Schreibfischs: Ich
dachte tatsächlich sehr lange, dass Vampir mit 'der' oder 'das' richtig ist. Vielleicht
hat sich mein jüngeres Selbst geweigert, zu akzeptieren, dass Vampir wieder
ein Begriff im Maskulinum ist, um menschenähnliche Wesen zu beschreiben. Jedenfalls
ist das nun ein In-Universe-Ding: In meinen Geschichten geht der oder das Vampir.]"

Anschließend vertauschten sie sozusagen ihre Emotionen, was Emre belustigte: Ranuk
wurde von Emres Enthusiasmus angesteckt und freute sich über das gefundene
Wort. Und Emres Enthusiasmus wich Verwirrung mit besagter Priese
Belustigung. "Welches Blut?"
