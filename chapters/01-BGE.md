BGE
===

\Beitext{Ranuk}

Es gab nun also Bedingungsloses Grundeinkommen, kurz BGE. So
stand es mit Blut an den Spiegel geschrieben, und was dort mit
Blut geschrieben stand, stimmte im Normalfall. Manchmal waren
die Nachrichten etwas tiefsinniger und ihre eigentlichen Bedeutungen
entfalteten sich erst nach einer gewissen Zeit und einigen
erleuchtenden Erlebnissen, aber an 'Ab nächsten Monatsanfang
gibt es BGE!' war nichts besonders tiefsinnig oder schwer
zu deuten.

"Aha", sagte Ranuk. Wer wusste schon, ob der Geist das
hörte, aber Ranuk sprach vorsichtshalber trotzdem mit
ihm.

Gaby hatte vorhin auch schon gesagt, dass die Sache mit dem BGE nun
wohl klappen würde. Aber Gaby fügte in jeden ihrer Sätze zu
politischen Themen ein 'wohl', 'vielleicht', 'sieht so aus' oder
eine andere verunschärfende Wortsammlung hinzu, die zu einer recht hohen
Wahrscheinlichkeit ihre Berechtigung hatte. Gaby war eine
weniger verlässliche Quelle als die in Blut geschriebenen
Nachrichten auf Ranuks Spiegel, von denen Ranuk eigentlich
keine Ahnung hatte, wer sie schrieb, also dieses 'wer' einfach
mit 'Geist' betitelte.

Ranuk erzählte niemandem vom Geist, aus Angst, irgendwer könnte auf die Idee
kommen, es wäre besser, ihn zu verjagen oder aber Ranuk zu therapieren oder so
etwas. Aber Ranuk war eigentlich recht zufrieden mit dem Geist. Eine Therapie
hätte Ranuk hingegen aus anderen Gründen gern gehabt. Aber Therapiesuche auf
dem Land, nun ja, stellt euch hier ein unbelustigtes Lachen vor, das sogar zu
müde ist, um wenigstens sarkastisch zu klingen.

Schon ohne zusätzliche Einschränkungen wäre so eine Suche eine Umöglichkeit
gewesen, aber Ranuk hatte auch kein Auto. Und Fahrradfahren war mit zunehmenden
Gesundheitsbeschwerden auch nicht immer möglich. Nur an guten Tagen und dann
nicht allzu weit. Das beschränkte die Optionen von erreichbaren Therapierenden
auf den Sakral-Vater im Nachbarort, und Ranuk war nicht unbedingt ein Fan jener
Religionsgemeinde. Der Sakral-Vater hatte eine schöne Stimme, aber vertonte damit eher
unschöne Dinge. Sein Hauptanliegen schien Versöhnung und Vergebung im
Diesseits und Jenseits zu sein. Ranuk hatte keineswegs ein Interesse, sich mit den verstorbenen
Eltern zu versöhnen. Nicht zuletzt, weil Ranuk gar kein Sohn war. Zu
'verkindern' war allerdings keine brauchbare Entgenderung des Begriffs
'versöhnen'. Auch egal. Ranuk hatte jedenfalls nichts dergleichen vor.

Wegen der Sache mit dem fehlenden Auto brachte Gaby Ranuk einmal in
der Woche den Einkauf. Der stand noch halb unausgeräumt in der Küche. Ranuk
hatte erst einmal verschnaufen müssen, weil das Gespräch angestrengt
hatte, und hatte sich gemütlich geduscht. Nach dem Verlassen der
Duschkabine war dann die BGE-Nachricht auf dem beschlagenen Spiegel gewesen, in
Blut, das sich allmählich in die vielen kleinen Tröpfchen auf dem
Spiegel auflöste. Ranuk lächelte. Das entbehrte nicht einer gewissen
Ästhetik. Der Fotoapparat stand griffbereit auf dem Schränkchen im Flur
neben dem Badezimmer für solche Fälle. Ranuk hatte eine Foto-Sammlung
auf dem Rechner, nur mit Sprüchen des Geists. Er nutzte nicht immer den
gleichen Spiegel. Und manchmal glaubte Ranuk, aus dem Schriftzug
eine Emotion ablesen zu können.

Wieder mit weicher Kleidung am Körper begab sich Ranuk in die Küche, um
den Einkauf auszuräumen. Das Eis sollte nicht noch länger außerhalb des
Kühlfachs herumschmelzen. Während Ranuk ausräumte, wollten die Erinnerungen
an das Gespräch mit Gaby verarbeitet werden. Ranuk mochte Gaby, aber
sie lebten schon irgendwie in sehr verschiedenen Welten, die eher
mäßig freiwillig Annäherungsversuche wagten. Heute hatte es so einen
gegeben: Gaby hatte sich noch einmal nach Ranuks Pronomen erkundigt. Und
hatte gefragt, ob sie Sätze bilden dürfte, um sie anwenden zu lernen. 'Rie, ihs,
ihs, rie', hatte Ranuk geantwortet, wie 'sie, ihr, ihr, sie', mit vertauschtem
'r' und 's'. Der Beispielsatz, den Gaby gewählt hatte, war gewesen: Ich küsse
rie auf ihse Lippen.

Daraus, dass Gaby das jetzt so genau wissen wollte, wagte Ranuk zu
schließen, dass Gaby sich über die vergangenen zwei Jahre, die sie die
Information zu Ranuks Neopronomen schon hatte, einen Scheiß dafür
interessiert hatte, und es nun einen Anlass gab, der das änderte.

Wobei, vielleicht war das ein bisschen hart. Vielleicht redete Gaby einfach
mit niemandem über rie und hatte daher die Pronomen in dritter Person
für Ranuk nicht gebraucht. Hatte sich das nun geändert? Und falls ja, warum? Und
falls nein, warum fragte Gaby das jetzt?

Ranuk hätte es vielleicht erfragen sollen, aber ihs Kopf hatte sich
unsinnigerweise lieber stattdessen mit wilden Hypothesen beschäftigt und
gleichzeitig versucht, sich nicht über die Beispielsätze und die Fragen aufzuregen.
Es war keineswegs bei dem einen Beispielsatz geblieben, irgendwelche
unangenehm verallgemeinerten Sätze über Personen, die Neupronomen benutzten,
waren auch gefallen. Alles ohne böse Absicht, und das machte für Ranuk
durchaus einen Unterschied. Aber rie fühlte sich immer verpflichtet, dann
möglichst verständlich und gleichzeitig präzise zu korrigieren, damit
Gaby eine mögliche andere Kontaktperson mit Neopronomen nicht bedrängte
oder unangenehm berührte oder ähnliches.

Ranuk atmete schwer und langsam ein und aus, bevor rie die letzten Dinge
im Kühlschrank verstaute und sich die Treppen hinauf schleppte. Auf dem
Spiegel im kleinen Flur am Kopf der Treppe hatte der Geist mit Blut eine Mohnblume
gemalt. Er machte Fortschritte mit der Malkunst. Er hatte recht grobmotorisch
angefangen. Ranuk hatte ihm irgendwann Pinsel zurecht gelegt, falls er
sie benutzen könnte. Aber sie veränderten nie ihre Position und waren
auch nie nass oder blutig. Auch die Malkunst wirkte nicht pinseliger, aber
niemand konnte leugnen, dass sich der Geist mehr Mühe mit Details gab. "Ich
mag die Mohnblume", sagte Ranuk.

Ihs Plan war nun, zur Entspannung ein wenig zu Zocken. Wobei Zocken vielleicht
ein zu elitärer Ausdruck für ein kreatives Sandkastenspiel sein mochte. Rie
setzte sich jedenfalls an den Schreibtisch des einzigen Zimmers, das rie sich
für sich selbst eingerichtet hatte, mit dem Plan, in Escube eine Reise durch
die Würfelwelt zu machen. Es gab eigentlich so viel zu tun, und rie hatte so
wenig Kraft. Es war das Haus ihser verstorbenen Eltern. Rie mochte das Haus an
sich und auch die Gegend, aber jeder Kubikzentimeter des Hauses und des
Grundstücks abgesehen von diesem Zimmer steckte noch voller Geschmack ihser
Eltern, voller unangenehmer Gefühle. Irgendwann wollte Ranuk daran arbeiten.
Aber wie das gehen sollte, wenn ein Gespräch mit Gaby rie schon so ausnockte, dass
erst Duschen und nun Escube spielen dran war, wahrscheinlich für Stunden,
wusste rie auch nicht. Der riesige Berg an Arbeit lähmte so sehr, dass
Ranuk nicht einmal anfangen konnte.

Bevor Ranuk die Escube-Welt öffnete, in der rie mit einigen enger befreundeten
Personen quasi lebte, sah rie wie gewohnt einmal die E-Mails und Chat-Nachrichten
durch. Einige Leute hatten sich einfach mit Herzchen gemeldet, weil sie an
Ranuk gedacht hatten. Ranuk fühlte sich warm dabei und schickte andersfarbige
Herzchen zurück. Ein schönes Ritual. Die E-Mail allerdings beinhaltete kein
Herz. Sie war von Gaby und bestand aus einen einzigen Link. Ach richtig, der Link, den
sie angekündigt hatte, erinnerte sich Ranuk. Was Gaby dazu erzählt hatte, hatte
Ranuk eher wenig überzeugt.

Rie seufzte und öffnete ihn doch. Er führte auf ein Arbeitgebenden-Portal, wobei
'arbeitgebend' in dem Fall, wie Gaby berichtet hatte, tatsächlich hieß, dass
die Leute, die sich darauf Profile anlegten, anboten, Arbeit zu
verrichten. Das Portal war für Leute gedacht, die sich im Wesentlichen mit
ihrem BGE finanzieren wollten, und nun Arbeit verrichten wollten, die ihnen
wirklich Spaß machte, mehr um der Arbeit willen und nicht so sehr für Geld. Es
klang an sich wieder nach so einer Sache, die Gaby vielleicht nicht richtig
verstanden und deshalb mit vielen 'vielleicht's und 'wohl's erklärt
hatte. Auch Ranuk verstand die Seite nicht gleich. Aber das Profil der
Person, die Gaby ihs verlinkt hatte, machte tatsächlich genau diesen Eindruck:

> Moin, ich bin Emre und ich habe einen BurnOut. Ich habe bisher
> in einem Bürojob gearbeitet, der mir an sich Spaß gemacht hat, aber
> seit geraumer Zeit kann ich einfach nicht mehr. Ich würde gern
> körperliche Arbeit verrichten, bei der ich zügig sehen kann, dass
> sie einen Effekt hat. Gartenarbeit zum Beispiel, oder Malern,
> Nageln, Sägen, so etwas. Ich habe allerdings keine Ausbildung
> in der Richtung.
> 
> Ich kann nicht versprechen, wie das mit dem BurnOut klappt. Ich
> möchte deshalb keinen festen Zeitplan haben und eher so
> halbtags arbeiten. Im Gegenzug zu dieser Unzuverlässigkeitskomponente
> möchte ich aber wenig oder kein Geld, besonders nicht, wenn die Arbeit für
> eine Person ist, die selber nicht wohlhabend ist. Ich möchte sie allerdings
> irgendwo im Norden verrichten, irgendwo am Meer und brauche eine
> Unterkunft. Ich bin sehr genügsam: Über den Sommer reicht mir ein
> Zelt mit Strom und Kochplatte und eine Möglichkeit, mich
> gelegentlich zu duschen. Wenn das
> finanziell mitgetragen werden kann oder auf dem Grundstück, wo ich
> arbeite, gestellt werden kann (Bauernhof vielleicht?), wäre das super.
> 
> Ich habe keine Ahnung, ob das nun ein ansprechendes Angebot ist, aber
> man kann das ja mal probieren.

Gaby hatte gemeint, das wäre vielleicht was. Damit hatte sie gemeint, Ranuk
solle sich einfach bei einer wildfremden Person melden, die ihs helfen sollte, das
Haus und vor allem das Grundstück zu was eigenem zu machen. Ranuk seufzte und atmete
noch einmal schwer ein und aus. Rie ließ niemanden ins Haus, und das
hatte rie Gaby auch gesagt. Rie hatte nicht gesagt, warum. Es war
ein kleines, chaotisches Haus, und Ranuk liebte Privatsphäre. Aber
vor allem fühlte rie sich überhaupt nicht wohl bei dem Gedanken, eine
Person in dieses Chaos aus offensichtlicher Nachlässigkeit, einem gewissen
Hang zum Trödelsammeln, Staub und alten, schlimmen Gefühlen zu lassen. Das
rie noch dazu mit einem Geist teilte, von dem niemand wissen sollte.

Aber Gaby hatte gemeint, rie solle das Gartenhäuschen zur Verfügung
stellen. Das wäre mit dem Wasseranschluss darin sogar besser als ein
Zelt. Eine Kochplatte hatte es noch nicht, und es war derzeit mit
sehr viel Kram zugestellt, von dem Ranuk einzig das eine Fahrrad, das
heilste unter den drei Rostgestellen, gelegentlich herausholte. Aber
Gaby meinte, dass diese Anzeige für sie so klang, als wäre das ein
Angebot, dass man so einer Person durchaus machen konnte. Erster Arbeitstag,
erstmal vollgesperrmülltes Gartenhaus mit Regenlecks in eine
Unterkunft verwandeln. Ranuk kam das unverschämt vor. Noch dazu
fiel es ihs schwer, fremde Leute überhaupt anzuschreiben. Rie schloss
den Link wieder und reagierte auf die Mail nicht mit einem Herzchen in einer
Farbe. Wobei, wenn rie auf Herzchen mit Herzchen reagierte, müsste rie
auf diese Mail korrekter Weise mit einem Link reagieren, aber auch
das tat rie nicht.

Als rie nach mehreren Stunden Escube langsam müde wurde, war
ihs das Hintergrundbild mit völlig zugewuchtertem Garten und eingefallenen
Holzhütten, das Emre für das Profil gewählt hatte, allerdings immer noch nicht aus dem Kopf
gegangen. Rie versetzte den Rechner in den Suspend und fragte sich, ob
rie noch die Kraft hatte, sich fürs Schlafen umzuziehen. Rie schleppte
sich ins Schlafzimmer. Nicht gerade ihs Lieblingsort im Haus. Das
Ehebett der Eltern, der verspiegelte Kleiderschrank der Eltern, den
rie noch nie gemocht hatte, weil er so monströs und schwer wirkte. Wobei
ein Spiegel in diesem Haus auch einen gewissen Vorteil hatte.

'Tu es!', stand in riesigen, ungeduldigen Buchstaben in noch nicht
getrocknetem Blut darauf. Das Licht der Straßenlaternen von
draußen spiegelte sich darin.

Das war dann eher eine der kryptischeren Nachrichten. Bezog sich das
auf die Frage, ob rie sich noch umziehen sollte, oder auf Emre?
