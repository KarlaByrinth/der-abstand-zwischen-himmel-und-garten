Entwicklung, Danksagung, Referenzen, Eastereggs und Metagedöns {.unnumbered}
==============================================================

Dieses Novellchen entstand, weil ich eine kurze Fantastik-Novelle mit
nicht-binärem Hauptcharakter schreiben wollte. Ich habe dafür Inspiration
in diesem Internet gesucht. Solange Twitter noch irgendwie steht, könnt
ihr den Aufruf nach Wünschen noch lesen:

[Link auf einen Tweet von mir auf Twitter mit der Frage nach Wünschen.](twitter.com/karlabyrinth/status/1532080400469721093)

Ich habe versucht, möglichst viele einzubetten. Vielen Dank an dieser Stelle für die Wünsche von

- Harlequin: Kuscheln, Pusteblumen.
- MoorFaun: Was macht eigentlich Flederschatten so, wenn fer nicht im Spiel ist? (Siehe dazu den abstrusen Epilog.)
- Ivi: Fliegende Quallen, Wanderung ohne bestimmtes Ziel, flauschige Sandbänke.
- Iris Leander Villiam: Urlaub, Geister, Pride, Muscheln.
- Aşkın-Hayat Doğan: Käsekuchen, Prota über 60 Jahre alt, Schnecken, Pizza, Albatros.

Es macht mir Spaß, große Mengen wirrer Wünsche auf eine Weise in ein Werk zu gießen, dass
es möglichst zusammenhängend wirkt.

Ria Winter hat das Werk testgelesen und intensiv kommentiert. Es ist mir immer wieder eine
riesige Freude, mit Ria Winter an Texten zusammenzuarbeiten. Ich bin stets ein großer Fan
davon, was dabei rauskommt, und ihre Bücher gehören definitiv zu meinen allerliebsten! (Ja, ich mache
in diesem Danksagungsgedöns schamlos Buchwerbung für die Feuervogel-Reihe!)

Bei diesem Testlesen kam heraus, dass die Sache mit Flederschatten vielleicht doch etwas
sehr meta ist. Deshalb schiebe ich das Kapitel für die besonders mutigen Leute in einen
Eventuellen Easteregg-Epilog hinter dieses Danksagungsgedöns. Wenn ihr bereit seid, euch eventuell
den Spaß zu verderben, verwirrt zu werden, ein bisschen Selbstkritik zu lesen
oder einen Hintergrund zu erkennen, den ihr
vor allem dann versteht, wenn ihr mehr Werke von mir lest, dann lest ihn. Sonst lasst ihn
gern liegen. Er ist optional.

Wie immer findet ihr alle Informationen zum Buch auf meiner Homepage:

[Link zur Seite des Buchs auf meiner Homepage.](skalabyrinth.org/books/DerAbstandZwischenHimmelUndGarten.html)

"Der Abstand zwischen Himmel und Garten" funktioniert unabhängig von meinen anderen Werken, aber
er kann auch als in der Myrie-Zange-Welt angesiedelt verstanden werden. Das ist eine zulässige, aber
keine notwendige Interpretation. Daher kommen allerlei Ortsnamen aus der Myrie-Zange-Welt auch
hier vor: Nyanberg, Fork, Geesthaven, Fjärsholm, Grennland, Skandern.

Das bedeutet, dass die hier vorkommenden Figuren nicht unbedingt Menschen sind. Ich habe kein einziges
Mal das Wort "Mensch" verwendet. Es ist eurer Interpretation überlassen, ob sie es sind oder nicht. Die
Frage ist allerdings auch: Macht es so einen Unterschied? Ist es überhaupt wichtig?

Ich verbaue oft Muster in Romanen, zum Beispiel in Kapitelnamen, die entdeckt werden können. Dieses
Mal ist es lediglich, dass immer abwechselnd zwei Kapitel aus Ranuks und eines aus Emres Sicht
geschrieben ist.
