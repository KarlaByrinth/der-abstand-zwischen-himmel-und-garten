Eventueller Easteregg-Epilog {.epilog .unnumbered}
============================

\Beitext{Flederschatten}

Flederschatten speicherte die Geschichte und beschloss, dass da
auch nicht mehr viel rauszuholen war. Es war ganz schön ins Düstere
abgedriftet, wie immer.

Emre war eine Person, die Flederschatten faszinierte und der
sha so eine Entspannungsphase mit einer
Person wie Ranuk gewünscht hätte. Natürlich
wusste Flederschatten nicht, ob Emre nicht irgendetwas Vergleichbares
gehabt haben könnte. Über Emres Privatleben neben dem Aktivismus
war nicht so viel bekannt.

Emre war vielleicht auch nicht die bekannteste, historische Person während der
Klima-Revolution gewesen, aber wohl die bekannteste, die in der Zeit out
als nicht-binär gewesen war.

Flederschatten war nur so mäßig zufrieden mit der Geschichte. Die
Klima-Kriege waren eben ein tristes Thema und Flederschatten schrieb
darüber eigentlich nicht sehr gern. Deshalb hatte der Text vielleicht
zu wenig Show und zu viel Tell entwickelt. Und irgendwie zu
wenig Glücksgefühle. Die Ziege war auch etwas kurz gekommen. Und
ob eine Geistergeschichte wirklich das Wahre
für das Setting war? Ob Leute bei Geistergeschichten nicht mehr Hintergrund zum
Geist wissen wollen würden? Deren Motivation?

Aber die Charaktere waren schön. Gaby hatte vielleicht auch zu
wenig Screentime gehabt. Emre in der Geschichte erzeugte in
Flederschatten aber immerhin das gleiche, humorig und doch
ernste Gefühl, wie Emre aus Berichten und
Übertragungen von damals. Oder aus Texten von Emre selbst. Und
Ranuk war vielleicht am ehesten ein Self-Insert. So ganz passte
das nicht, aber das war vielleicht auch besser so. Die Ziege
hatte Flederschatten eingebaut, weil auch in shanem Leben eine Ziege eine Rolle
gespielt hatte, die sha sehr gemocht hatte. Aber jene hatte
vielleicht mehr Charakter gehabt.

Flederschatten überlegte, wen sha die Geschichte beta-lesen lassen
würde. Vielleicht würde ja Mirash mögen.
