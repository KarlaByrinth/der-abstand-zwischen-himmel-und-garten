Wellt
=====

\Beitext{Ranuk}

Es war Herbst geworden und die Stürme rüttelten an der Hecke und am
Baum. Herbst hieß auch, dass es nachts wieder Nacht wurde und tags noch
Tag war. Sofern ein mit Wolkendecken überzogener Himmel, deren
Schatten über den Boden wehten, herkömmlich auch als
Tag bezeichnet würde. Ranuk mochte es jedenfalls.

Sie saßen abends noch draußen, wenn es dunkel wurde, und
sahen sich das Himmelsspektakel über dem Garten an. Der
Garten war schön. Unter dem Baum war der Stammplatz des Tisches, aber
um in den Himmel zu sehen, hatte Emre ihn heute samt Gestühl mitten auf den
Wiesenbereich gestellt, der regelmäßig gemäht wurde. Der Bereich war
etwa zwei- bis dreimal so groß wie Ranuks Schneise damals, also
nicht allzu viel Arbeit. Auf der einen Seite gab es ein kleines
Steinbeet mit Rinkeln und Ranuen. Auf der anderen begrenzte ein
kleines, verwunschenes Mäuerchen ein Beet, dass auf der einen
Seite mit insektenfreundliche Blumen gepflanzt war, vielen
Sorten, und auf der anderen mit Kräutern. Außerdem hatten sie
tatsächlich in einem Teil des Gartens das Gras einfach
stehen gelassen, dass nun ungestört gen Himmeln wuchern durfte. So etwas
hatte Ranuk immer schön gefunden und Ranuks Eltern immer
grässlich.

Die Ziege hatte sich vor ein paar Stunden schon vom Acker
gemacht. Nun lagen sie zu zweit in den Liegestühlen und schauten in
das sich ständig verändernde Geleucht im Himmel. Im Moment waren nur
ein paar Puffwölkchen da, die vom Mond angeleuchtet wurden. Und
etwas Aura. Beides zusammen tanzte über den Himmel wie dezent bunte
Quallen.

Alles kam und ging in Wellen. Die Wolken, die Jahreszeiten, die
Tage. Und nicht zuletzt die Wellen am Meer, die ja auch Wellen
waren. Wellen gehörten zu Leben so sehr, dass Emre das Wort
Welt am liebsten mit zwei l geschrieben hätte, hatte
Emre erzählt. Wobei es das Wort 'wellt' ja auch wirklich gab. Dritte
Person Singular, Präsens von 'wellen'.

"Es ist wirklich eine schöne Wellt geworden, die wir hier
geweltet haben", sagte Emre weich und genießend, die
Arme hinter dem Kopf im Liegestuhl verschränkend. Keine
nackten Arme immerhin. Emre trug seit einer Woche etwa
langärmlige Ringelhemden.

"Es ist immer was zu tun, aber ich habe allmählich trotzdem
ein Fertig-Gefühl", sagte Ranuk.

Die Farben wellten für ein paar Momente leuchtender
am Himmel. Die Sterne dazwischen fühlten sich so
fern an, und gleicheizig waren sie noch viel weiter
weg, als sie sich anfühlten. Das war ein schönes Gefühl
von Raum. Ranuk fühlte sich gern klein, im Vergleich
zu dieser Welt. Wellt. Diesem riesigen Universum.

"Ist es ein Erschöpft-Gefühl, so ein, du bist völlig
fertig? Oder ein zufriedenes Etwas geschafft haben
Gefühl?", erkundigte sich Emre.

"Vor allem letzteres, aber auch ein bisschen das erste. Wie
ist es bei dir?" Ranuk spürte eine untergründige Angst, die
rie zu verdrängen versuchte, als rie das fragte. Und schämte
sich gleichzeitig.

"Dasselbe. Ich fühle mich viel weniger erschöpft, als als
ich kam", sagte Emre. Und fügte hinzu: "Ich mag grammatikalisch
richtige Sätze mit zwei älsen."

Diese angenehm entspannte Stimme, dachte Ranuk. "Heißt
das, dass du bald gehen wirst?" Das war die Angst.

Emre drehte den Kopf kurz zu Ranuk, aber blickte dann wieder
in den Himmel. "Ich würde schon gern noch ein oder zwei Wochen
bleiben. Versuchen, Energie zu tanken, während die Welt
da draußen vor sich hin distopiert." Traurigkeit oder
Frust schlich sich in Emres Stimme. "Aber ich werde nicht
anders können, als die gewonnene Kraft dann zum Kämpfen
zu verbraten. Da tobt ein Krieg, ich muss da rein."

Es tobte ein Krieg. Über Grenlannd vor allem im Moment. Grenlannd
war ein kleiner Kontinent, der mit seinem ganzen Wald und Grün
eine wesentliche Komponente im Klimagleichgewicht spielte. Konzerne
und sowas hatten herausgefunden, dass es dort Ressourcen gab, -- natürlich. Die
sie gewissenlos abbauen wollten, dazu den Kontinent abholzen und
anschließend für Anbau und Tierzucht verwenden wollten.

Klimaaktivist\*innen hatten versucht, den Wald zu
besetzen. Unerfolgreich bisher, weil die dort heimische Tierwelt sich
dagegen sträubte, den Lebensraum zu teilen. Ranuk
fand das sehr verständlich. Während es zugleich Hoffnung für
Grenlannd reduzierte.

Weniger pazifistische, politische Gruppen versuchten, die Wirtschaftselite
mit etwas mehr Erfolg mit Waffen aufzuhalten. Und eine etwas esoterisch
wirkende Vereinigung hatte sich gegründet, die Mondzeugen, die Wege
suchten, mit den vor Ort lebenden Tiervölkern doch zu verhandeln, dass
sie dort zum Schutze des Kontinents vorsichtig siedeln dürften. Sie
versuchten, die Tiere als sie selbst anzusehen und nicht so zu tun,
als wären sie ihnen allzu ähnlich. Ihre Methoden
wirkten nicht sehr wissenschaftlich, aber sie hatten erste Erfolge, sodass
sie schon an zwei Stellen auf dem Kontinent Camps aufgeschlagen
hatten, und auch noch fast alle lebten.

Emre spielte seit längerem mit dem Gedanken, sich anzuschließen, aber
so ganz überzeugt war Emre von der Gruppe nicht. Emres Einstellung
zu den Mondzeugen war allerdings erheblich freundlicher als das
Bild, das viele Medien zu vermitteln versuchten. Wie sie
in den vor allem staatlichen Medien dargestellt wurden, trug eher dazu bei, dass
sich über nebensächliche Eigenschaften der Gruppe aufgeregt wurde, die
nicht so in ein normiertes Gesellschaftsbild passten, und vielleicht
sogar manchmal ethisch fraglich waren. Es sorgte für
eine Verurteilung der Mondzeugen durch den größten Teil der Bevölkerung, die in
keinem Verhältnis zu irgendwas stand, was gerade eigentlich hätte
wichtig sein sollen.

Emre war nicht sicher, ob eine so vertretbare Idee wäre, sich den
Mondzeugen anzuschließen, dass es sich für Emre gut anfühlte, aber wollte zumindest
Aufklärungsarbeit gegen die Diskursverschiebung leisten. Und sich an
Demonstrationen und anderem zivilen Ungehorsam beteiligen.

Sie sprachen nicht allzu viel darüber. Emre hatte nicht ohne Grund eine
Pause von alledem gebraucht.

"Wenn du wieder ausbrennst, darfst du hier immer gern auftanken, wenn
das taugt", versprach Ranuk.

Dieses Mal blickte Emre länger zu ihs hinüber, mit einem Lächeln
im Gesicht. "Ich überlegte ohnehin schon, ob ich dich frage, ob ich
den Sommer über immer ein paar Monate hier sein kann. Ich kann bei
zu heißen Temperaturen nicht denken. Was bringt mir der Wille zu
Aktivismus, wenn ich nicht denken kann und wenn ich keine Ruhepausen
bekomme und deshalb zu kaputt gehe."

Ranuk nickte und ließ sich allzu freiwillig von dem Lächeln
anstecken. "Herzlich gern. Hast du dadurch irgendwelche
Probleme mit Wohnsitz oder sowas?"

Emre seufzte. "Ich versuche möglichst, Dinge unter der Hand zu
machen. Die Gesetzgebung ist beschissen und nicht für mich
geeignet", sagte Emre. "Ich bin salvenisch, übrigens. Also
habe Wurzeln in einem Volk, das eigentlich mal ein reisendes
Volk war. Und traditionell Wohnraum in verschiedenen Regionen hatte, der
halt Saison-weise bewohnt wurde. Das ist lange her, dass wir
das so haben leben können. Das wurde auf
dem ganzen Kontinent Maerdha über die letzten Jahrhunderte immer mehr
unterdrückt. Meine Urgroßelter-Generation ist daher nach Arelis
ausgewandert, wo es damals noch lockerer war, aber in Arelis
wurde nachgezogen. Und weil es zwischendurch nach Änderungen
der Gesetze zu unseren Gunsten in Maerdha aussah, sind
meine Eltern, die noch gefühlt vage Erinnerungen von ihren Großeltern
hatten, wie es mal war, vor meiner Geburt wieder
hierher zurück-ausgewandert. Wir hatten aber eben gezwungenermaßen einen
festen Wohnsitz. Meine Eltern haben versucht, mir in Urlauben
etwas über meine Wurzeln zu zeigen, aber es ist alles so
zerrissen, dass ich eigentlich kein Gefühl dafür habe. Ich bin
in einer sesshaften Kultur groß geworden und komme aus einer
reisenden, und weiß überhaupt nicht, was für mich richtig
wäre."^[Hier wird sehr rasch sehr dicht viel zu Emres Hintergrund
erzählt. Für mich ist das logisch: Emre hat einen BurnOut und sperrt
alles eine ganze Weile einfach komplett aus. Aber es entwickelt sich
für Emre ja nicht langsam, sondern in dem Moment, in dem es wieder
ein bisschen geht, rasselt das halt rein. An Personen, die irgendwie
hiermit relaten, sei es wegen BurnOut oder sei es, weil anderes hier sie
repräsentiert: Wie findet ihr das? Sollte ich das eher streichen? Schadet
das so, wie ich das hier mache?]

<!--Vor einem Print die Fußnote entfernen.-->

Ranuk hörte geduldig zu, bis Emre ausgeredet hatte. Rie fand
es interessant, aber ihs war gleichzeitig bewusst, dass darin
ein Schmerz steckte, den rie nicht nachempfinden können würde. Rie
wusste nicht, wie rie darauf angemessen eingehen könnte und
sagte deshalb erst einmal ein paar Takte nichts. Und dann: "Ich
mag dich. Und ich wünschte, du könntest es besser oder leichter
haben." Rie holte einmal tief Luft und vertiefte den
Blick in die Weite des Himmels. "Ich bin wohl ziemlich sesshaft. Ich
möchte hier am liebsten nie wieder weg und der Gedanke daran,
das zu sollen, fällt mir schwer. Ich würde vielleicht auch gern
an Aktivismus beteiligt sein, aber ich kann es vor allem vom
Computer aus, und das ist nur so mäßig effektiv."

"Ich glaube, das ist effektiver, als du denkst", sagte
Emre leise. "Von dem, was du erzählst, und wie ich
dich erlebe, bist du gut darin, Leuten Halt zu geben. Und
es ist so wichtig, irgendwo einen Halt zu haben, gesagt
zu bekommen, dass eins nicht falsch ist. Das gibt mir überhaupt
die Kraft, kämpfen zu können."

"Ach Emre", seufzte Ranuk. "Du bist so gutmütig
und motivierend und genügsam. Klar ist das ein bisschen
geben, aber wenn die Mehrheit ist wie ich, wird das nicht
reichen."

"Das Problem ist vor allem, dass eine Minderheit nicht so
ist wie du, und sich dafür einsetzt, Strukturen zu
behalten, die allen weh tun", gnarfzte Emre. "Können
wir das Thema wechseln?"

---

Emre hatte ein ganz gutes Verhältnis zum Geist entwickelt. Oder der
Entität. Natürlich hatte Emre sich die selbe Frage gestellt wie
Ranuk anfangs: Woher kommt das Blut? Oder der Geist selbst?

Ranuk war vor allem erleichtert, dass Emre auch Nachrichten
bekam. Eine gewisse Angst hatte Ranuk schon gehabt, dass der
Geist sich vor Emre verstecken würde, sodass Emre Ranuk nicht
glauben würde. Aber es entstanden Nachrichten auf den
Spiegeln, während sie beide draußen waren, die Emre zuerst
entdeckte. Emre zweifelte nicht. Oder behauptete das zumindest.

Nach den ersten Nachrichten an Emre hatte Emre kurzerhand
geantwortet, und zwar nicht direkt etwas sagend, wie Ranuk
das getan hatte, sondern Emre hatte mit roter Tusche Nachrichten
unter die des Geistes geschmiert. Mit Ranuks Einverständnis
natürlich.

Emre hatte zwar nicht aus dem Geist herausbekommen, wo das
Blut herkam, aber demm immerhin dazu bringen können, die
rote Tusche selbst zu benutzen. Um den kleinen, roten Tuschfarbenbehälter
entstand in ihrer Abwesenheit oft viel niedliches Gekleckere.

Heute Abend wollten sie einen Film gucken, und als der Geist
davon mitbekommen hatte, hatte dey gegen Mittag eine Empfehlung
dagelassen. Aber entgegen Ranuks Haltung, dem Geist oft nachzugeben,
lehnte Emre ab, weil sie eigentlich beide nur mäßig Lust auf
den Streifen hatten. Ranuk hätte ihn gewählt, weil ihnen die
Entscheidung schwer fiel, welcher es dann werden würde. Aber
Emre hatte einfach die vier Titel, auf die sie am ehesten
Lust hatten, unter dem Vorschlag des Geistes auf den Spiegel
geschrieben, und deren Vorschlag durchgestrichen.

Als sie nun das Haus betraten, weil es inzwischen viel zu kalt
geworden war, hatte der Geist freundlicherweise nur einen
stehengelassen. Eiswelt. Passend zu Ranuks Schlotterzustand, der
aber einer warmen Decke und einem Heißgetränk nachgeben würde.

Sie setzten sich also zusammen auf das Sofa. Ranuk hielt mutig
einen Arm nach oben, und zu ihser Überraschung verstand
Emre sofort und kuschelte sich hinein.

Der Film ging um eine Prinzessin mit magischen Eis-Kräften, die
mit ihrem Schiff und einem schwangeren, männlichen Pinguin über
das Eismeer schlidderte, um die Welt vor einer Hitzewelle
zu retten.

Ranuk mochte ihn, weil er so schön darstellte, dass eine Bedrohung
auch sehr real da war, wenn sie völlig unsichtbar war und
erst viel später wirksam würde. Und weil auch während so
einer Aktion Alltag und andere Probleme vor sich
hinpassierten.

Ranuk schlief auf dem Sofa ein, die Arme um Emre geschlungen. Es
war alles sehr seltsam. Gerade war es gut, aber eigentlich
war auch nichts gut. Seltsam eben.
