Winden
======

\Beitext{Ranuk}

"Wenn wir zusammen baden wollen, dann sollten wir bald
reingehen, denn noch ist mir vom Fahren warm genug", stellte
Ranuk klar.

"Geht ihr ma' ohne mich." Gaby breitete die Picknickdecke
auf dem Felsen aus, der sachte zum Wasser hin abfiel und
angenehm warm war, und steckte eine Mini-Regenbogenflagge
am Stiel in einen kleinen Spalt daneben. Sie hatte den
Ort wohl deshalb ausgesucht, damit sie dort stecken konnte.

"Gehörst du zu den Leuten, die am liebsten eigentlich doch überredet
und etwas metaphorisch in den Hintern getreten werden wollen?", fragte
Emre.

Gaby lächelte. "Nein, ich fühle mich hier auf der Decke ganz
wohl. Geht ihr ma' ohne mich."

"Dann los!", forderte Emre Ranuk auf.

Emre zauderte nicht viel und zog sich einfach aus. Latzhose,
Top und Unterwäsche. Emres Körper war an den Armen in den vergangenen
Wochen nachgebräunt. Die Füße waren sehnig. Ranuk mochte
sehnige Füße, stellte rie abermals fest.

Rie hatte Emre nicht lange angesehen, für die Eindrücke hatte
ein rascher Blick genügt. Ranuk hatte keine Ahnung, wie sehr
sich das gehörte, hinzusehen oder absichtlich wegzugucken. Sie
hatten im Vorfeld verabredet, dass sie nackt baden würden, aber
für Ranuk war das letzte Mal Nacktbaden etwa zwanzig Jahre her, und
auch damals war rie nicht unbedingt lässig dabei gewesen. Rie
überwand sich und wand sich aus der Kleidung. Ihs Körper sah
auch sehr anders aus als vor zwanzig Jahren. Das Gewebe war
an manchen Stellen des Körpers weniger strukturiert oder so.

"Magst du es?" Wieso fragte Ranuk das? Wie war das passiert?

"Was meinst du?", fragte Emre freundlich.

Ranuk strich über die Haut am Oberarm, die sich besonders anders
anfühlte als zu Zeiten, zu denen Selbstakzeptanz noch einfacher
gewesen war. "Diese angeknitterte Haut und das Gewebe."

"Sehr!", betonte Emre.

Ranuk blickte Emre verwirrt an. Dass Emre etwas Motivierendes
sagen würde, war ihs klar gewesen, das tat Emre immer, aber
das? "Dein Ernst?"

"Ja, auf jeden!" Emre grinste. Das war kein Ausdruck, den
Emre oft verwendete, aber am ehesten, wenn Emre sehr überzeugt
und gut gelaunt war. "Ich habe so etwas schon immer gemocht. Vielleicht,
weil ich ein Herz für Aussehen habe, das oft negativ wahrgenommen wird. Ich
habe es vielleicht zunächst aus Mitleid gern, und dann dekonstruiert
sich dabei mein verinnerlichtes Schönheitsdenken oder sowas und
anschließend mag ich es dann einfach so sehr gern. Ich glaube, das hat sich
ergeben, weil ich mit dem Orchester damals in Altenheimen gespielt
habe, und die anderen Mitglieder der Band sich darüber aufgeregt
haben, dass alte Leute Körper zeigen. Das fand ich schlimm und
daneben. Ich glaube, da habe ich gelernt, es wertzuschätzen und
nun ist es einfach da, das Mögen." Emre stutzte und fügte
weniger elanreich hinzu: "Trete ich dir mit irgendwas davon
zu nahe oder erzähle unangenehme Dinge?"

Ranuk schüttelte den Kopf. Ihse Gedanken fühlten sich etwas
wirr an, aber negativ war es nicht. "Vielleicht hast du schon
recht, und das sind alles Ideale, an denen wir arbeiten können, bis
wir was anderes schön finden, aber es ist manchmal sehr
schwer", sagte rie.

"Bei sich selbst nochmal schwerer als bei anderen, glaube ich. Man
vergisst einfach nicht, mit welchem Blick die da draußen uns ansehen
und werten", erwiderte Emre. "Ich bin inter, und Leute wissen
zwar vielleicht nicht, was los ist, wenn sie mich ansehen, aber sie
wissen schon, dass das irgendwie nicht stimmig ist für sie. Das
spüre ich schon. Bei dir irgendwie bisher aber nicht."

Ranuk lächelte. Endlich fühlte rie sich Emre gegenüber mal nicht
furchtbar schuldig. Ein Zittern, das ihsen Körper dazu brachte, sich
zu winden, weil der Wind einmal kalt über die warmen Steine wehte, brachte
rie zurück zu den Plänen. Die Knie taten vom Radeln immer noch ein
wenig weh, aber nicht so schlimm wie neulich.

Emre hatte das Tandem wieder in Gang gekriegt. Einfach falls dabei
etwas nicht klappen würde, hatten sie ausprobiert, ob es im Zeifel
auf Gabys Auto montiert werden könnte. Das hatte Ranuk genug
Sicherheit gegeben, mit Emre zusammen ans Meer zu radeln, in
gemäßigtem Tempo. Und das hatte auch ganz gut geklappt. Nun
war ihs warm und ein wenig erschöpft, aber noch motiviert genug, um
die Füße ins Wasser zu stecken, oder ein wenig mehr.

Ranuk tappste vorsichtig über den warmen Stein, während
Gaby das Picknick vorbereitete und nicht weiter auf sie beide achtete. Gaby
hatte im Moment auch ziemlich viel mit sich selbst zu tun. Sie hatte sich
kürzlich als pansexuell geoutet. Ursprünglich als bi, aber im Gespräch
mit Emre und Ranuk hatte sie herausgefunden, dass sich pan noch
besser anfühlte.

Ihr Mann war seid zwei Jahren tot. Seitdem hatte
sie gelegentlich versucht, zu daten, aber wenig Erfolg gehabt. Erst
hatte sie nur Männer zu daten versucht, aber dabei irgendwie herausgefunden,
dass es ihr egal war, welches Geschlecht die Person hatte. Irgendwas
hatte sie dann von einer Pride in Høppla geredet, wo sie Emre
und Ranuk hätte mitnehmen wollen, aber Emre und Ranuk waren beide nicht
so die Party-Personen. Also hatte Gaby eine Mini-Pride nur zu
dritt vorgeschlagen, und hatte damit ein Picknick am Meer
gemeint. Nun waren sie hier. Und das Regenbogenfläggchen, ein sehr
einfaches, wehte im Wind.

Gaby hatte ihr Lebtag nicht so sehr damit gerechnet, queer zu
sein, sich lange nicht dazugezählt und nun herausgefunden, dass sie
es selbst war. Das beschäftigte sie, meinte Emre. Emre hatte mehr
ein Gefühl für sowas als Ranuk.

Emre wartete geduldig im fast knietiefen Wasser ab, bis Ranuk
gefolgt war. Dann fragte Emre einfach: "Willst du händchenhalten
beim Weiterreingehen?" Mit so einem funkelnden Grinsen im Gesicht.

Ranuk hatte damit nicht gerechnet, aber der Gedanke fühlte sich
ziemlich schnell gut an. Also reichte rie Emre die Hand. Und gemeinsam
wateten sie langsam ins tiefere Wasser. Ranuk war sicher, dass
Emre in Nullkommanix drin gewesen wäre, hätte Emre nicht
auf rie Rücksicht genommen. Es war egal. Ranuk ertastete mit den Füßen den Stein unter
sich, bis sie zu der Stelle kamen, wo feiner, weicher Sand ihn
bedeckte, der dort liegen blieb, weil wasserstoffblonde Algenhärchen
ihn dort gefangenhielten. Es war weicher, flauschiger Boden und
Ranuk wusste nicht, ob es so einen noch einmal irgendwo auf
der Welt gab. Es war so schön, ihn doch noch einmal im Leben
zu spüren. Und vielleicht würde es nun mit Emre häufiger
passieren, die Überwindungshürde niedrig genug sein.

---

Später saßen sie auf der Decke auf dem Stein, Ranuk in eine zusätzliche
Decke gewickelt, die Gaby zum Glück mitgebracht hatte. Gaby versuchte
Smalltalk, aber war nicht enttäuscht, dass weder Emre noch
Ranuk darin besonders gut waren. Sie aßen Käsekuchen dazu, dieses
Mal selbstgebackenen von Gaby. Sie hatte aus dem Kauf geschlossen, dass
das eine Sache war, die ihnen wohl schmecken würde. Und hätte kaum
mehr recht haben können!

"Der Kuchen ist eine Wucht", warf Ranuk sachlich ein. Also, eigentlich
nicht sachlich, aber alles, was Ranuk sagte, klang irgendwie sachlich, sogar
für rie selbst.

"Das freut mich!", erwiderte Gaby.

Ein paar Momente herrschte Ruhe, abgesehen vom Möwengeschrei und
leisen Mampfgeräuschen. Ranuk wünschte sich einen Stuhl oder so
etwas. Auf dem Boden sitzen ging zwar gerade, aber würde unangenehme Nachwirkungen
haben.

"Du gehst gar nicht mehr gern auf Demos, Emre, oder?", fragte Gaby.

Emre lächelte, aber es wirkte wie Masking, wie ein Lächeln, was
da nur war, weil Emre ein positiveres Grundgefühl auslösen
wollte, und schüttelte den Kopf. "Du hast irgendwelche
Informationen über mich gehabt, noch bevor ich hierher kam,
richtig?"

"Ich habe deine Anzeige damals gefunden, die ich dann Ranuk
weitergegeben habe, weil ich dachte, das könnte was werden
mit euch", erklärte Gaby.

"Was werden?", fragte Ranuk.

"Mit dem Garten, meine ich. An was anderes habe ich nicht
gedacht", behauptete Gaby.

Ranuk versuchte sie auf diese Weise zu beobachten, bei der
sie meistens einknickte und noch Informationen hinzufügte. Es
funktionierte auch dieses Mal.

"Also, ich dachte, ihr könntet auch persönlich zueinander
passen und euch was zu sagen haben", fügte sie hinzu. "Du
hast ja außer mir keine Outernet-Kontakte, und ich weiß, dir
geben die Internet-Kontakte genug, das haben wir ja
mal diskutiert, aber als ich dir die
Anzeige wegen Gartenarbeit empfohlen habe, ließ mich einfach
nicht los, muss ich zugeben, dass ich dachte, dass du dich mit
Emre vielleicht weniger alleine fühlen könntest. Weil,
ja, ich weiß nicht, wie ich das richtig ausdrücke, aber ihr seid
halt beide irgendwie nicht Mann und nicht Frau oder sowas."

Ranuk fühlte sich mit der Ausdrucksweise nicht unbedingt völlig
wohl, aber verzichtete auf eine Korrektur. Es war für rie zumindest
dicht genug dran und Gaby wusste, dass sie sich nicht
präzise ausdrückte, sondern nur eine ungefähre Informationskategorie
in ungenauen Worten anriss. Mehr war gerade auch nicht wichtig.

"Woraus wurde das Gedöns ersichtlich aus der Anzeige?", fragte Emre. "Oder
hast du das einfach geschlossen, weil eben keine Information zu
Geschlecht dabei stand? Außer meinem Namen vielleicht, der klassisch
eher männlich gelesen wird."

"Dein Name, also spezifisch dein Name, und die Signatur auf
dem Foto, das kannte ich halt noch", erklärte Gaby.

Emre seufzte tief und unterbrach damit Gabys Anstalten,
fortzufahren. "Da habe ich versäumt, die Signatur zu
entfernen. Klimaaktivismus", sagte Emre. "Deshalb frugst
du nach Demos. Weil ich solche mal angestiftet habe und etwas
bekannter in der, wie nennt eins das, Szene war."

"Und du hast aus irgendwelchen Gründen aufgehört", stellte
Gaby fest.

"BurnOut", sagte Emre schlicht und seufzte dann noch einmal tief. "Ich
kann nicht mehr." Es klang eher wie ein einzelnes, als wie vier
Wörter. "Es stresst auch ungemein" -- Emre unterbrach den Satz zeitgleich
mit dem Atemfluss, sodass es besonders abgehackt klang --, "Karren
gegen die Wand fahren, nein mit Karacho dagegen breschen, ihr
wisst, jedenfalls, lasst uns über was anderes reden."

Ranuk überlegte, dass Gaby vielleicht deshalb wegen der Pronomen
gefragt haben könnte. Aber fragte nicht nach, weil es gerade
wichtig war, das Thema zu wechseln.

Leider fiel ihs nicht so sonderlich viel ein, deshalb wiederholte
rie: "Der Kuchen ist eine Wucht!" Was halt auch einfach stimmte.

---

Ranuk und Emre beschlossen, auch den Heimweg mit dem Rad zurückzulegen. Nach einem
Verdauungsspaziergang mit zu viel Kuchen im Bauch an der Wasserkante
entlang, wo sie Muscheln sammelten. Emre bückte sich sehr viel
häufiger. Es gab hier ziemlich hübsche Muscheln und einige weiße
Schneckenhäuser mit vielen kleinen Spitzen am Gewinde. Von letzteren
nur wenige. Sie hatten Glück heute, dass sie vier fanden.

Sie waren gerade angefahren und hatten das Meer noch in
Sichtweite, als ein riesiger Albatros über sie
hinwegsegelte. Ranuk wusste, dass es sie hier irgendwo gab, aber hatte
trotzdem noch nie einen gesehen.

Wahrscheinlich war er für seine Verhältnisse nicht riesig. Albatrösse
waren doch alle recht groß.

"Wow!", sagte Emre. "Du gönnst mir hier die unrealistischsten, schönsten
Erlebnisse!"

"Du bist unrealistisch!", erwiderte Ranuk. "Wie kann eine Person wie
du real sein?"

Es blieb eine Weile ruhig. Vielleicht lächelte Emre, aber Ranuk konnte
das nicht sehen. Ein Teil von ihs hatte auf einmal Angst, dass Emre sich
nach dieser Frage einfach in Luft auflösen könnte. Und dann wäre Ranuk
allein auf einem Tandem. Aber die Vorstellung ergab keinen Sinn. Wenn
Emre nur Vorstellung war, dann auch das Fahren auf einem Tandem. Dann
würde das Tandem mitverschwinden.

"Eine imaginäre Person würde an meiner Stelle nun womöglich
etwas ähnliches sagen wie eine reale", stellte Emre schließlich
fest. "Ich könnte versuchen, dich davon zu überzeugen, dass ich
real bin. Aber wenn du willst, dass ich real bin, würde ich dasselbe
als imaginäre Person auch tun."

Die Antwort war sehr Emre, fand Ranuk. Real oder nicht, Emre
war ein gelungender Charakter. Nicht nur vom Nettigkeitsgrad
her, sondern auch so insgesamt. "Ich mag dich", sagte
Ranuk also.

"Ich dich auch."

---

Später daheim im Haus kochte Ranuk ihnen beiden in der
Küche einen Soppen. Es ging wieder. Die Küche gehörte nun
Ranuk. Sie hatten sie gemeinsam aufgeräumt und geputzt, und
weil sie das noch nicht eigen genug gemacht hatte, hatte
Emre die Schränke darin abgeschliffen und fliederfarben gestrichen. Sie
wollten noch schöne Queer-Pride-Aufkleber auf die
Schränke kleben, aber erst, wenn die Farbe ein paar Tage
durchgehärtet wäre. Ranuk konnte sie noch riechen.

---

Es war nicht wichtig, was real war. Das war ohnehin
Ermessenssache. Es war wichtig, was zuverlässig Sinn
und Halt im Leben gab.

Zum Leben hatte seit jeher Kultur gehört, und Kultur bestand ebenso seit
jeher zum Teil aus sehr ausgedachten Dingen wie Theater, Tanz, Musik,
Geschichten. Dinge waren nicht weniger wert dadurch, dass sie imaginär
waren. Das halbe Leben bestand aus Imaginärem, was eben am Leben
hielt. Leute, die so versessen darauf waren, Realem mehr wert
zuzusortieren, dachten meistens nicht zu Ende.

Bei Geschichten verschwamm das alles besonders. Geschichten
waren wichtig. Geschichten mit Charakteren. Für
Ranuk waren Buchcharaktere schon immer mindestens so wichtig gewesen wie
reale.

Eine imaginäre Freundschaft war nicht zwangsläufig weniger wichtig als
eine reale, oder gar automatisch in irgendeiner Weise schlecht.

Wenn eine imaginäre Freundschaft die Kraft und Sortiertheit gab, einen
ganzen Garten umzubasteln und eine Küche umzugestalten, einen
Raum für sich zu erobern und sich selbst mehr zu genießen, dann
war sie, im Gegenteil, sehr viel wert.

Es spielte vielleicht schon eine Rolle, ob Emre real war oder nicht, für
die Anspielung mancher Sinne, aber es war in jedem Falle besser, als Emre
nicht zu haben.

Ranuk betrat das Schlafzimmer, das sie noch nicht umgestaltet hatten. Auf
dem Spiegel stand in roten, blutigen Buchstaben: 'Du hast gar nicht
so viel Humor'.

"Ey!", beschwerte sich Ranuk.

Rie brauchte ein paar Momente, bis sich eine mögliche, freundlichere
Deutung der Worte in ihsen Gehirnwindungen ergab: Ranuk hatte in
der Tat nicht so viel Humor. Rie hätte einen Charakter wie Emre
vielleicht gar nicht erfinden können. Oder doch?
