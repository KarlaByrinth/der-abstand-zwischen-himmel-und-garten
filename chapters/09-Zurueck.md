Zurück
======

\Beitext{Emre}

Zurück in den Klimaaktivismus, und auch anderen, eigentlich
egal welchen, hieß auch, wieder am laufenden Band öffentlich
misgendert zu werden. Einen Artikel schreiben, ein Interview
geben, eine Rede halten, hinterher wurde Emre zitiert und zwar
immer mit dem selben falschen Pronomen und den selben
falschen Wortformen. Dann hieß es: Gibt es nicht wichtigere
Dinge? Und ja, verslikte Axt, Emre war Klimaaktivismus
wichtiger. Aber wie viel mehr Energie könnte Emre darauf
verbraten, wenn Berichtende dererseits ihre Energie nicht
darauf verbraten würden, sich nicht mal eben von anderen korrigieren zu
lassen. Emre war längst zu müde dazu. Für sich selbst. Für
andere korrigierte Emre jedes Mal. Gegen zu große und
zu viele Windmühlen. Die für die Energiegewinnung fehlten.

Dieser ganze Unsinn war so auslaugend. In jeder freien Stunde verzog
sich Emre in Gedanken in Ranuks Garten zurück. Ein Traum, der
irgendwo aus quasi nicht-existenter Quelle noch Energie
spendete. Emre war da ja im Moment nicht wirklich. Fantasien
und Träume, ohne die nichts ging. Emre sehnte sich jeden Tag
mehr. Zurück nach Hause.
