Plan Los
========

\Beitext{Ranuk}

Ranuk schaute abermals aus dem Fenster in den Garten hinab. Rie
mochte das Wort 'abermals'. Das war das Gute an dem Satz. Das
Schlechte war, dass es rie ziemlich nervös machte. Emre arbeitete
dort unten den Plan ab. Entgegen Ranuks Überlegungen, mitzumachen,
saß rie hier einfach herum und tat nichts. Nicht einmal Escube
zocken. Weil rie sich irgendwie verpflichtet fühlte, zu helfen, sich
aber nicht aufraffen konnte.

Emre hatte zunächst das Schuppendach geflickt und nun arbeitete
Emre unermüdlich unter dem Baum, um dorthin eine schöne Sitzecke
zu zaubern. Sie hatten zuerst überlegt, eine Laube für die Sitzeecke
zu bauen, was hübsch gewesen wäre. Aber dazu hätten sie Steine verlegen
müssen, weil Ranuk es andernfalls kaum hätte selbst pflegen können. Ständig
zum Rasenmähen den Tisch zu versetzen, war mit 67 und nicht übermäßig
fit nicht drin, oder zumindest nicht mehr lange.

Um Steine zu verlegen, hätten sie Steine gebraucht, und außerdem
hätten sie den Boden versiegelt, was nun nicht die beste
Sache im Zusammenhang mit Naturschutz war. Vielleicht waren für
den kleinen Flecken die Gedanken übertrieben, aber sie hatten ihn
beide gehabt. Also hatten sie beschlossen, das Gras doch zunächst
stehen zu lassen.

Unter dem Baum wuchs weniger, weil er so viel Schatten dahin warf. Der
Boden war dort recht fest, sodass die Beine des Tisches nicht einsinken
würden. Schatten war ohnehin angenehm. Nur die Vögel schissen dort
eben hin, deshalb hatte Ranuk zunächst an eine Laube gedacht. Aber in einer
Laube würden sich wohl auch Vögel einnisten.

Emre versuchte, den Boden unter dem Baum ausreichend zu ebnen und
zu hübschen. Ranuk hatte nicht so viel Fantasie und wäre vermutlich
eh im Weg gewesen. Und schließlich hielt rie es nicht mehr aus. Einfach
weggehen. Einen Spaziergang machen. Das hatte rie schon lange nicht
mehr getan.

Rie packte sich ein wenig Obst als Proviant ein und füllte sich
im Bad eine Flasche Wasser ab. Das Wasser im Bad schmeckte besser
als das in der Küche. Natürlich sah rie die fein säuberliche,
mahnende Schrift schon, als rie das Bad betrat, aber las sie
erst, als die Flasche voll war.

'Du solltest die Sache gestern oder heute Umplotten. Ist da nun ein
Flecken Gras gesenst, wo der Tisch stand, oder nicht?'

"Aber ich mochte die Pizza-Szene!", beschwerte sich Ranuk.

An sich hatte der Geist recht. Die Handlung war inkonsistent. Ranuks
Leben fühlte sich ohnehin schon länger etwas inkonsistent an. Und das
frustrierte rie. Noch ein Grund mehr, einfach mal wegzugehen.

---

Der frische Wind strich über Ranuks Arme und rie war, als hätte rie
ihn schon ewig nicht mehr gefühlt. Er war angenehm auf der Haut und
fühlte sich im Gegensatz zu
allem anderen recht real an. Im Haus fühlte sich auch Escube real an, aber
mit dem Wind auf der Haut wusste Ranuk, dass es ein anderes real war. Escube
war eine Spiel-Welt zum Spazieren ohne Schmerzen. Oder zumindest mit weniger
Schmerzen. Aber eben auch ohne Wind auf der Haut. Ranuk atmete ihn tief
ein. Er war so echt, so hier. Rie würde ihn leider wahrscheinlich
nicht lange wahrnehmen. Üblicherweise spazierte ihs
Körper irgendwann von alleine und startete das Fantasieren von
Geschichten. Abenteuer, die ihs passieren könnten. Wenn rie nicht
aufpasste, dann nicht unbedingt gute. Eher welche, in denen Leute
rie fertig machten und sagten, dass rie nicht dazugehörte. Also versuchte
rie aufzupassen und schöne Geschichten zu erfinden. Und wenn sie
schön genug waren, schrieb rie sie auf, um auch andere daran zu erfreuen. Aber
viele der Geschichten waren zu persönlich, weil rie selbst darin vorkam. Wenn
rie solche doch mal aufschrieb, mussten sie dann ein wenig angepasst werden.

Vielleicht half gegen das Wegdriften in Geschichten, einen Weg zu wählen,
den Ranuk noch nicht kannte. Weil dann der Automatismus ihsen Weg an
den Kreuzungen und Gabelungen nicht von alleine wählen könnte.

Rie sah in den Himmel hinauf. Aus gegebenem Anlass stellte rie sich
vor, wie das Gras in ihsem Garten so hoch wuchs, dass es in die
Wolken stach, und wie diese dadurch beim Weiterziehen in Streifen
zerflirrten.

Die Straße hinunter gab es einen schmalen Weg durchs Feld, den sich Ranuk
schon immer einmal in der Karte hatte ansehen wollen, es aber immer
verpeilt hatte. Den wählte rie nun. Vielleicht führte er zum Meer. Vielleicht
endete er einfach mitten im Feld. Rie mochte nicht, wenn der Hinweg der selbe
war wie der Rückweg. Sonst hätte rie ihn längst probiert. Aber vielleicht
ging es, ihn hin zu gehen, wenn an sich schon die Idee wäre, auf dem Rückweg einen
anderen zu wählen, und wenn sich dann rausstellte, dass da keiner war, eben
damit zu leben.

Der Weg führte zwischen einem Labusterfeld, das orange blühte,
und einem Getreidefeld entlang. Ranuk wusste nicht, was es für ein
Getreide war. Irgendwo weiter hinten musste eine Heiderosenhecke die
Felder trennen. Ranuk sah sie noch nicht, aber der Geruch wehte
unverkennbar durch die Sommerwärme zu ihs hinüber.

Der Anfang des Weges war wirklich schön. Auch wenn Ranuk sich ärgerte, als
rie eine wunderschöne Neinbergschnecke den Weg kreuzen sah, keinen
Fotoapparat dabei zu haben. Das war noch ein Grund, warum rie schon eine Weile
nicht spazieren gegangen war: Rie wusste, dass die Routinen lange eingerostet
waren, einzupacken, was rie brauchte. Dass irgendetwas fehlen würde, und
dass rie sich dann ärgern würde, weil das früher alles geklappt hätte. Aber
rie war gut genug gelaunt aufgebrochen, dass nach dem Ärger noch ein
bisschen Energie übrig blieb, um weiterzuspazieren. Nachdem rie die
Neinbergschnecke beim Kriechen zu Ende beobachtet hatte. Rie hatte
sich immer gefragt, ob es auch Jabergschnecken gab. Oder warum Neinbergschnecken
so hießen. Damals als Kind und jugendliche Person, als rie noch kein
Internet gehabt hatte, um es nachzuschauen. Und heute war es oft
so, dass rie sich an die vielen Fragen von damals nicht mehr erinnerte, oder
die damalige Akzeptanz, es nicht mal eben recherchieren zu können, noch
heute eingerastet war.

Rie holte den Taschenrechner heraus und fand einen lustigen Artikel, dass
es zwar keine Jabergschnecken, durchaus aber Wahbergschnecken gab. Aber
rie merkte, wie bei der Helligkeit, die es brauchte, damit rie auf dem
Gerät lesen konnte, der Akku sehr schnell leerer wurde. Und vielleicht brauchte
rie den Taschenrechner später noch zum Telefonieren.

Der Feldweg machte einen Bogen und mündete in einer T-Kreuzung, wo Ranuk
den Weg wählte, von dem rie vermutete, dass er eher zum Meer führte. Aber
so richtig sicher war rie sich nicht. Irgendwann als rie beim besten
Willen nicht mehr wusste, in welche Richtung das Meer lag, weil
es bewölkt und daher nicht so klar war, wo die Sonne eigentlich war, suchte
rie sich die Wege nach Schönheit aus. Den einen, der durch den kleinen
Wald führte, weil es dort schattig war. Dann eine Sackgasse, die sich zwischen
zwei Gräben im Feld entlang gewunden hatte. Und irgendwann ließ sich
der Schmerz in den heißer und dicker werdenden Knien nicht mehr
verdrängen.

Das war gar nicht mal so gut. Also, Schmerzen waren ohnehin nie
gut, aber rie fragte sich ernsthaft, ob rie es noch bis nach
Hause aushalten würde.

Ranuk machte sich im Kopf ein Bild davon, wo rie entlanggelaufen
sein mochte, und versuchte darin die größere Straße zu orten. Rie
vermutete, dass diese einigermaßen in der Nähe sein und einen
weniger beschwerlichen, weil asphaltiert, und
direkteren Weg nach Hause darstellen müsste. Und das war an sich auch
eigentlich nicht falsch. Außer, dass rie sich mit der
Entfernung verschätzt hatte. Der Pfad durchs Feld mit den
hohen Halmen links und der Spinurbel rechts zog sich lang und länger, bis
Ranuk fast aufgab und überlegte, doch den gleichen Weg zurückzugehen. Auf
der Straße zu gehen war ohnehin nicht so der Hit, was die
Ästhetik anging. Aber es ergab sich dieses Spiel: Nur noch
bis zu dieser Anhöhe, vielleicht sehe ich
ja von dort die Straße, und wenn nicht, drehe ich um. Nur noch bis
zu dieser Kurve, vielleicht verdeckt das Feld die Straße nur, und wenn
ich sie dann noch nicht sehe, drehe ich um. Und so weiter.

Als Ranuk endlich an der Straße ankam, sackte rie am Straßenrand zusammen
und kühlte mit den eigentlich warmen Händen (weil sie in der Tasche
geweilt hatten) die Knie. Rie hätte die Knie vermutlich mit einer
durchschnittlich warmen Wärmflasche kühlen können. An Aufstehen
war nicht so richtig zu denken. Auf dem Boden sitzen war an sich
auch nicht wirklich gut, aber stehen kam noch viel weniger in
Frage. Rie fragte sich, wie lange rie
hier sitzen müsste, bis es wieder ginge. Ein paar Stunden? Oder eher
ein paar Jahre? Solange würde Ranuks übriges Trinken nicht reichen. Es
war nicht mehr viel.

Ein weiterer Blick auf den Taschenrechner verriet, dass der Akku
auch nur noch etwa eine Stunde reichen würde, wenn Ranuk das Gerät nicht
übermäßig viel benutzte.

Ranuk seufzte. Das Schamgefühl war nicht gerade zurückhaltend, als
rie Gaby anrief.

"Klar hole ich dich ab!", versprach diese, als sie verstanden hatte, wo
Ranuk ungefähr war. "Dass du die Strecke geschafft hast, ist aber echt
schön zu hören, so an sich. Wenn du mehr spazieren würdest, wenn ich
dich wo hinfahre und abhole, sag Bescheid, das mache ich gern."

Gaby war ja durchaus sehr nett, fand Ranuk. Aber eine Geschichte mit
einer Person wie Gaby hätte Ranuk wohl eher nicht geplottet. Gaby hatte
diese Art von sehr bemüht, aber es oft nicht so richtig
hinkriegend, respektvolle Worte zu wählen. Das wäre in Geschichten
ungerecht: Leute würden sie vermutlich eher als unangenehmen
Charakter einsortieren, das hätte sie nicht verdient, fand Ranuk.

Als das Auto an der Feldwegmündung hielt, rappelte Ranuk sich mühsam
hoch. Gaby war ausgestiegen, um ihs zu helfen, aber das wollte
Ranuk nicht, also beeilte rie sich, obwohl es so beschwerlich
war. Rie sortierte sich auf den Beifahrendensitz und schnallte
sich an. "Danke."

"Wie gesagt, immer gern!", versprach Gaby. "Wobei abgesprochen
ein bisschen besser wäre. Wenn es nicht dringend spontan sein
muss, so wie jetzt. Ich bin ja auch nicht immer da."

Das stimmte nicht, hatte Ranuk den Eindruck. Gaby war wirklich
immer da. Sie arbeitete irgendwas im Home Office.

"Und du lässt Emre wirklich nicht ins Haus?", fragte Gaby.

"Fast nicht", gab Ranuk zu.

"Braucht Emre nie ein Badezimmer?" Gaby machte ein belustigt,
alberiges Gesicht.

Es ermutigte Ranuk irgendwie dazu, die Wahrheit zu sagen. "Emre
verbindet sich die Augen zum aufs Klo gehen."

"Nicht dein Ernst." Entgegen des Inhalts der Ellipse, die Gaby
da aussprach, klang sie allerdings wieder ziemlich ernst.

"Das war so abgemacht. Schon im Vorhinein.", verteidigte sich
Ranuk. Und schon war dieses Schamgefühl wieder da.

"Das kannst du nicht machen!", stellte Gaby klar. "Ich versteh'
dich ja mit dem Haus und Privatsphäre und so. Aber irgendwann
geht das auch zu weit! Meinst du nicht?"

Ranuk reagierte nicht, sondern blickte aus dem Fenster. Gleich
würde die Fahrt auch schon wieder vorbei sein.

"Emre ist ziemlich genügsam und versucht es anderen sehr
recht zu machen, habe ich den Eindruck", fuhr Gaby fort, als
ihr wohl das Schweigen zu lang wurde. "Hast du nicht wenigstens
über sowas wie ein Chemie-Klo nachgedacht? Die Dinger gibt es
auch in ziemlich klein und nicht allzu teuer."

"Wir haben darüber gesprochen, finden aber beide, dass die
umwelttechnisch nicht so der Hit sind, und fanden die Variante
mit der Augenbinde besser", sagte Ranuk.

Das entsprach der Wahrheit. Und Ranuk hätte an Emres Stelle
auch so entschieden. Ja, Ranuk fühlte sich mies wegen der
schlechten Besuchsfreundschaft, die rie lebte. Aber auf der
anderen Seite konnte rie Emre schon nachvollziehen.

"Emre ist da wirklich genügsam. Ich frage mich, ob, äh, Emre
nicht mehr für, äh, die eigenen Rechte eintreten sollte", überlegte
Gaby, weniger energisch als vorhin. "Wie sind eigentlich
Emres Pronomen?"

"Keine Ahnung.", sagte Ranuk.

"Ist das nicht das, worüber ihr nicht-binären Leute immer
als erstes redet?", fragte Gaby irritiert.

Sie ordnete Emre also als nicht-binär ein. Das war
interessant.

"Es sollte auf jeden Fall unabhängig davon sein, ob jemand nicht-binär
ist oder nicht, über Pronomen als erstes zu reden.", erklärte
Ranuk. "Warum hast du Emre nicht gefragt?"

"Aber warum hast du Emre nicht gefragt?" Gaby tat irgendwie so, als wäre
es viel unlogischer, dass Ranuk nicht gefragt hatte, als dass
sie nicht gefragt hatte, obwohl ihr Emre beim Abholen zuerst
begegnet war.

Aber die Frage war trotzdem interessant: Warum hatte Ranuk nicht gefagt?

Ranuk versuchte sich zu erinnern. Es war einfach anfangs nicht wichtig
gewesen, weil sie miteinander und nicht mit anderen übereinander geredet
hatten. Und die Scham über die ganze Hilfssituation
war so dominant gewesen, dass Ranuk aus
etwaigen Konzepten gebracht gewesen war. Die Nachfrage-Routine
war im Urlaub gewesen. Hinzu kam, dass Ranuk gerade
in einer Phase war, in der rie in manchen Situationen
ausprobierte, ob andere die Frage zuerst
stellten. Und dann hatte rie es vergessen. Und nun fühlte es sich
seltsam an, dies nachzuholen. Aber vielleicht sollte rie.

Sie hielten vor Ranuks kleinem Haus.

"Überleg dir, ob du nicht ein bisschen entgegenkommender sein
kannst. Ich glaube, das hätte Emre verdient", ermahnte Gaby
sehr freundlich. "Und wenn du irgendwo hingefahren werden
möchtest, und sei es mit Emre ans Meer oder so, sag Bescheid."

Ranuk versuchte sich mit einem Lächeln, bedankte sich noch einmal
und schleppte die alten, rostigen Knochen aus dem Auto. Es war
weggefahren, da hatte Ranuk die Haustür noch nicht erreicht.

---

Nach der Einnahme eines halben Liters Wasser, eines Schmerzmittels und
einer Scheibe Brot, damit das Schmerzmittel nicht so allein im
Magen zersetzt wurde, ging es Ranuk besser. Auf dem Spiegel im
Flur stand 'Käsekuchen?' in kleineren, aber um so flehenderen
Lettern. Das Blut war längst getrocknet. Käsekuchen hätte wirklich was.

Ranuk überlegte, sich nach oben zu schleppen, aber ihs Blick
wanderte in den Garten. Der sah ganz anders aus. Rie wanderte langsam
durch das Erdgeschoss bis zur Glastür, die zum Garten hin offen
stand. Emre hatte alles gesenst, was sie abgesprochen hatten, und
das Gras zusammengeharkt. Es sah nun viel freier aus, als wäre
ausreichend Platz im Garten, um zu sein. Emre selbst lag auf dem Rücken auf
der frisch frei gemähten Fläche und schlief. Die Sense lag neben
Emre im Gras. Auf Emres Kopf lag ein Strohut, der die Augen beschattete.

Es war ein schönes Bild. Ranuk hätte es gern fotografiert, aber
das wäre ihs übergriffig vorgekommen. Stattdessen sammelte rie
Zeichenpapier und Kohlestifte zusammen. Emre schlafend zu
zeichnen, kam Ranuk interessanterweise weniger übergriffig vor. Vielleicht
weil rie dabei wiedererkennbare Elemente aussparen konnte, also
Emre nur eine Zeichenvorlage war.

Obwohl Ranuk lange nicht mehr gezeichnet hatte, war das Zeichenmaterial
nicht tief verstaut oder schwer zu finden. Es war eine Priorität, es
für den Fall der Fälle greifbar zu halten.

Die Mamseln zwitscherten und planschten in einem von Emre bereitgestellten
Wasserbecken, das sie im Schuppen gefunden hatten. Ranuk ließ sich in
den Liegestuhl nieder und malte vorsichtig die ersten Linien. Rie
malte das erste Bild recht zügig und ein zweites sehr langsam, detailliert
und ausführlich. Rie fühlte sich beruhigt und entspannter, und viel mehr
im Hier, durch das Beobachten der Atembewegungen.

Es raschelte im Gras und das Geräusch klang nicht so viel anders
als das Rascheln von Papier. Es war Ate, die Ziege, die über
den Zaun gehopst war und sich nun durch das Gras neugierig näherte. Ate war
auch lange nicht mehr dagewesen. Früher, als Ranuks Eltern noch hier gewohnt
hatten, war sie oft hier gewesen, und dann noch eine Weile, bis das
Gras zu hoch geworden war und Ranuk zu selten draußen.

Ate war eine seltsame Ziege. Sie verließ die Ziegenherde, die nebenan oft
graste, immer wieder, um sich andere Orte anzuschauen, und die
Anwesenheit von Zweibeinern aufzusuchen.

"Nicht wecken!", flüsterte Ranuk, als sich die Ziege Emre näherte.

Aber die Ziege scherte sich nicht darum. Sie stuppste Emre vorsichtig
mit der Schnute an und meckerte. Emre fuhr auf und erschreckte sich. Aber
obwohl der Atem noch eine Weile vom Schrecken sehr schnell ging, war da nichts als
Freude in Emres Gesicht, als Emre Ranuk und die Ziege bemerkte.

Ranuk kramte ein anderes Blatt hervor, um Emre und die Ziege beim
Interagieren zu zeichnen.

"Malst du mich?", fragte Emre.

Ranuk nickte. "Ist das okay?"

Emre grinste und nickte auch. "Wenn ich darf, mag ich hinterher sehen."

Irgendwie war es ein gutes Gefühl, es nun mit Erlaubnis
weiter zu tun. Es war ein seltsamer Tag, aber insgesamt war er sehr gut. Dinge
wurden wieder.
