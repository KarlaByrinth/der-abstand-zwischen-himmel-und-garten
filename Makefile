SHORT_TITLE = DAZHUG
CHAPTERS := \
	01-BGE\
	02-MutwilligeZumutungen\
	03-Umzug\
	04-Pizza-Spaetstuecke\
	05-PlanLos\
	06-Begeistert\
	07-Winden\
	08-Wellt\
	09-Zurueck\
	EntwicklungDanksagungReferenzenEastereggs\
	Epilog
INPUTS := $(CHAPTERS:%=chapters/%.md) metadata.yaml
OUTPUTS := DerAbstandZwischenHimmelUndGarten.pdf DerAbstandZwischenHimmelUndGarten.epub DerAbstandZwischenHimmelUndGarten.tex DerAbstandZwischenHimmelUndGarten.html ContentNotes.tex $(SHORT_TITLE)/ContentNotes.md $(SHORT_TITLE)
SPBUCHSATZV := SPBuchsatz_Projekte_1_4_9
#SPBUCHSATZV := SPBuchsatz_Projekte_1_4_6
#SPBUCHSATZV := SPBuchsatz_Projekte_1_4__Pre_Beta_11
#SPBUCHSATZV := SPBuchsatz_Projekte_1_3

all: $(OUTPUTS)

DerAbstandZwischenHimmelUndGarten.html: $(INPUTS)
	pandoc \
		$(INPUTS) \
		-o $@


DerAbstandZwischenHimmelUndGarten.epub: $(INPUTS) ContentNotes_epub.md
	pandoc \
		-t epub2\
		--css epub.css\
		-L SPepub.lua\
		--epub-cover-image=img/DerAbstandZwischenHimmelUndGarten1000x.png\
		$(INPUTS) \
		ContentNotes_epub.md\
		-o $@

DerAbstandZwischenHimmelUndGarten.tex: $(INPUTS)
	~/.local/bin/pandoc \
		--top-level-division=chapter \
		-t SPBuchsatz.lua \
		$(INPUTS) \
		-o $@

ContentNotes.tex: chapters/ContentNotes.md
	~/.local/bin/pandoc \
		--top-level-division=chapter \
		-t SPBuchsatz.lua \
		$^ \
		-o $@

ContentNotes_epub.md: chapters/ContentNotes.md
	@echo >$@ 'Content Notes'
	@echo >>$@ '============='
	@echo >>$@ ''
	@cat $< >>$@


# changed Ifstr to ifstr in SPBuchsatz_System/Helferlein/Hilfsfunktionen.tex 
DerAbstandZwischenHimmelUndGarten.pdf: DerAbstandZwischenHimmelUndGarten.tex ContentNotes.tex TitleAuthor.tex
	cp DerAbstandZwischenHimmelUndGarten.tex \
		deps/$(SPBUCHSATZV)/Buch/tex/Haupttext.tex
	cp ContentNotes.tex \
		deps/$(SPBUCHSATZV)/Buch/tex/ContentNotes.tex
	cp TitleAuthor.tex \
		deps/$(SPBUCHSATZV)/Buch/tex/TitleAuthor.tex
	cd deps/$(SPBUCHSATZV)/Buch/build/ ; \
		lualatex ../tex/Buch.tex
	cp deps/$(SPBUCHSATZV)/Buch/build/Buch.pdf DerAbstandZwischenHimmelUndGarten.pdf

$(SHORT_TITLE)/titlelist.txt: Makefile
	mkdir -p $(shell dirname $@)
	@echo '$(CHAPTERS)' | xargs -n 1 | cat -n >$@

get_chapter_number = $(shell \
	grep -E '^[[:digit:][:space:]]+$(1:chapters/%.md=%)$$' $(SHORT_TITLE)/titlelist.txt | \
		awk '{ print $$1 }' \
)


chapters-epub/%.epub: chapters/%.md $(SHORT_TITLE)/titlelist.txt
	pandoc \
		-t epub2\
		--css epub.css\
		--metadata title="$(call get_chapter_number,$<) - $(shell \
			head -n 1 '$<' \
		)"\
		$< \
		-o $@

$(SHORT_TITLE)/%.md: chapters/%.md $(SHORT_TITLE)/titlelist.txt
	@echo '$@'
	@echo >$@ '---'
	@echo >>$@ 'layout: $(SHORT_TITLE)-chapters'
	@echo >>$@ 'page-category: story-chapters-$(SHORT_TITLE)'
	@echo >>$@ 'title: "$(call get_chapter_number,$<) - $(shell \
			head -n 1 '$<' \
		)"'
	@echo >>$@ 'number: $(call get_chapter_number,$<)'
	@echo >>$@ 'lang: de'
	@echo >>$@ '---'
	@echo >>$@ ''
	@cat $< >>$@

$(SHORT_TITLE)/ContentNotes.md: chapters/ContentNotes.md
	@echo '$@'
	@echo >$@ '---'
	@echo >>$@ 'layout: $(SHORT_TITLE)-chapters'
	@echo >>$@ 'page-category: story-chapters-$(SHORT_TITLE)'
	@echo >>$@ 'title: "Content-Notes"'
	@echo >>$@ 'number: 0'
	@echo >>$@ 'lang: de'
	@echo >>$@ '---'
	@echo >>$@ ''
	@cat $< >>$@

$(SHORT_TITLE): $(CHAPTERS:%=$(SHORT_TITLE)/%.md) ;

clean:
	rm -rf $(OUTPUTS)

.PHONY: clean $(SHORT_TITLE)
